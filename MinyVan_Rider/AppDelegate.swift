//
//  AppDelegate.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GoogleSignIn
import Google
import FBSDKLoginKit
import FacebookCore
import TwitterKit
import TwitterCore
import Fabric
import Crashlytics
import IQKeyboardManagerSwift
import UserNotifications

var deviceTokenApp = "User cancel to register"
var newWindow: UIWindow?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
     var deviceTokenString: String!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
         registerForRemoteNotification()
        TWTRTwitter.sharedInstance().start(withConsumerKey:"hTpkPVU4pThkM0", consumerSecret:"ovEqziMzLpUOF163Qg2mj")
        
         //self.logUser()
        //Twitter.sharedInstance().start(withConsumerKey:"hTpkPVU4pThkM0", consumerSecret:"ovEqziMzLpUOF163Qg2mj")
        
//        <array>
//        <string>twitter</string>
//        <string>twitterauth</string>
//        </array>

        IQKeyboardManager.sharedManager().enable = true
        // Fabric.with([Crashlytics.self])
       //AIzaSyDpVCjWYDmjZMFC4QqhtV8izZpM7jC9zUc
        GMSServices.provideAPIKey("AIzaSyA01QwvYQYIsA5X8AlZirl3laQX3XRje50")
        GMSPlacesClient.provideAPIKey("AIzaSyA01QwvYQYIsA5X8AlZirl3laQX3XRje50")
        
        // client id:-  209547858886-jh17av1fm5i2vtiai4t8vidr98jpn5e4.apps.googleusercontent.com
          GIDSignIn.sharedInstance().clientID = "209547858886-jh17av1fm5i2vtiai4t8vidr98jpn5e4.apps.googleusercontent.com"
        // server id:- 209547858886-h3h1vbvsuraiubf6jn1gppo5c973uqs8.apps.googleusercontent.com
         GIDSignIn.sharedInstance().serverClientID = "209547858886-h3h1vbvsuraiubf6jn1gppo5c973uqs8.apps.googleusercontent.com"
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
       CommonFunctions.loadTokenOnAppLoad()
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        newWindow = window
       // handleView()
        return true
    }
    
    func handleView(){
        
        let leftViewController = {
            return UIStoryboard.viewController(identifier: "LoginViewController") as! LoginViewController
        }()
        
        newWindow!.rootViewController = leftViewController //
        newWindow!.makeKeyAndVisible()
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("hiplapl@gmail.com")
        Crashlytics.sharedInstance().setUserIdentifier("hiplapl")
        Crashlytics.sharedInstance().setUserName("Test User")
    }
    
    
    func twitterLoginBtn(){
        
//        let logInButton = TWTRLogInButton(logInCompletion: { session, error in
//            if (session != nil) {
//                print("signed in as \(session.userName)");
//            } else {
//                print("error: \(error.localizedDescription)");
//            }
//        })
//        logInButton.center = self.view.center
//        self.view.addSubview(logInButton)
    }
    
    func twitterLogin(){
        
//        Twitter.sharedInstance().logIn(completion: { (session, error) in
//            if (session != nil) {
//                print("signed in as \(session.userName)");
//            } else {
//                print("error: \(error.localizedDescription)");
//            }
//        })
//        //Request User Email Address
//
//        let client = TWTRAPIClient.withCurrentUser()
//
//        client.requestEmail { email, error in
//            if (email != nil) {
//                print("signed in as \(session.userName)");
//            } else {
//                print("error: \(error.localizedDescription)");
//            }
      //  }
//        //Token Expiration
//        TWTRAPIClient().loadTweet(withID: "20") { tweet, error in
//            if error.domain == TWTRAPIErrorDomain && (error.code == .InvalidOrExpiredToken || error.code == .BadGuestToken) {
//                // can manually retry by calling Twitter.sharedInstance().logInGuestWithCompletion
//            }
//        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("device messaging id.....\(deviceTokenString)")
        
        forwardTokenToServer(deviceTokenString)
        deviceTokenApp = deviceTokenString
        
    }
    
    func forwardTokenToServer(_ token: String){
        
        UserDefaults.standard.setValue(token, forKey: "Token")
        print("Device Token  : \(token)")
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote notification support is unavailable....\(error.localizedDescription)")
        
    }
    
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    // print("sdfghjk")
    //    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = ",notification.request.content.userInfo)
        //presentNextView(notification)
//        let content = UNMutableNotificationContent()
//        content.title = "Alert for notification"
//        content.body = "This is a demo notification"
//        content.categoryIdentifier = "newCategory"
//
//        let fileURL: URL = URL(string: "https://www.google.co.in/maps/@30.7291761,76.6816172,14z?dcr=0")!
//
//        let attachement = try? UNNotificationAttachment(identifier: "startLocation", url: fileURL, options: nil)
//        content.attachments = [attachement!]
//
//        let request = UNNotificationRequest.init(identifier: "newNotificationRequest", content: content, trigger: nil)
//
//        let center = UNUserNotificationCenter.current()
//        center.add(request)
        
        let imageDataDict:[String: UIImage] = ["image": #imageLiteral(resourceName: "startLocation")]
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: imageDataDict)
        // `default` is now a property, not a method call
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
        
        completionHandler([.alert, .badge, .sound])
        
    }
    
    // handle notification
    func showSpinningWheel(_ notification: NSNotification) {
        print(notification.object ?? "")
        if let dict = notification.object as? NSDictionary {
            if let id = dict["image"] as? UIImage{
             print(id)
        }
      }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print(response)
//        let responseString = response.notification.request.content.userInfo["completeMessage"] as! String
//
//        let contents = responseString.components(separatedBy:":" )
//        print(contents)
//        let type =  contents[0]
//        print("type of notification is:- \(type)")
//        let id = contents[1]
//        print("id of notification is:- \(id)")
//
//        let message = contents[2]
//        print("notification message is:- \(message)")
//
//        if type == "\(NotificatnType.DRIVER_15_MIN)"{
//            print("type is :- \(type)")
//            presentNextView(tripId: id)
//        }
//        if type == "\(NotificatnType.DRIVER_30_MIN)"{
//              print("type is :- \(type)")
//            presentNextView(tripId: id)
//        }
//        if type == "\(NotificatnType.DRIVER_5_MIN)"{
//              print("type is :- \(type)")
//            presentNextView(tripId: id)
//        }
//        if type == "\(NotificatnType.RIDE_STARTED)"{
//              print("type is :- \(type)")
//            presentNextView(tripId: id)
//        }
        
        
    }
    
    func presentNextView(tripId: String){
        
        let vc = {
            return UIStoryboard.viewController(identifier: "SingleDetailViewController") as! SingleDetailViewController
        }()
        vc.fromNotification = true
        vc.tripId = tripId
        newWindow!.rootViewController = vc
        newWindow!.makeKeyAndVisible()
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    func registerForRemoteNotification(){
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
                 UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
          FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let twitterDidHandle = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
                                                                sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                annotation: options[UIApplicationOpenURLOptionsKey.annotation])

        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])

        return facebookDidHandle || googleDidHandle || twitterDidHandle
       
    }

}

