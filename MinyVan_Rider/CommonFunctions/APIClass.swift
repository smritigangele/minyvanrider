//
//  File.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

class APIClass{
    
    static let base = "http://52.207.251.95:8090/api/v1/"
    //http://52.90.224.63:8090/api/v1/";//"http://192.168.0.25:8090/api/v1/" //
    
    // Account urls:-
    
    static let authenticate = APIClass.base + "authenticate";
    static let register = APIClass.base + "account/register/user/";
    static let change_password = APIClass.base + "account/change_password";
    static let forgotPassword = APIClass.base + "account/reset_password/init";
    static let resetPassword = APIClass.base + "account/reset_password/finish";
    
    // Add Device:-
    static let addDevice = APIClass.base + "device/add";
    
    // user-profile urls:-
    
    static let getProfile = APIClass.base + "account";
    static let editProfile = APIClass.base + "user/";
    static let searchMember = APIClass.base + "user/member/search?searchText="
    
    //client packages urls:-
    
    static let clientPackages = APIClass.base + "clientPackage/select/";
    static let getPackages = APIClass.base + "package/";
    static let commuterPackages = APIClass.base + "package/select/";
    static let myPackages = APIClass.base + "clientPackage/status/";
    static let sharedWithMe = APIClass.base + "clientPackage/sharedWithMe?page=0&size=20"
    static let sharedByMe = APIClass.base + "clientPackage/sharedByMe?page=0&size=20"
    static let searchPackages = APIClass.base + "clientPackage/sharePackage"
    
    //rider packages urls:-
    
    static let riderPackages = APIClass.base + "trip/rider/package/COMMUTER/"
    static let riderDetail = APIClass.base + "trip/id/"
    
    //active trips urls:-
    
    static let activeTrips = APIClass.base + "trip/tripStatus/PENDING/"
    
    //payments for booking trip:-
    
    static let generateToken = APIClass.base + "braintree/generate/token"
    static let paymentTransaction = APIClass.base + "braintree/transaction"
    static let bookTrip = APIClass.base + "trip/book"
    static let bookMultipleTrips = APIClass.base + "trip/book/multiple"
    
}
