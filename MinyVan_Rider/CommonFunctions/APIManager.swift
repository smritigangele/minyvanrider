//
//  APIManager.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class APIManger{
    
    static func login(view: UIViewController, login: LoginVM, completion: @escaping (TokenDTO) -> Void) {
        CommonFunctions.post(url: APIClass.authenticate, parameters: login.toJSON(), view: view, authRequired: false, completion: completion)
    }
    
    static func signup(view: UIViewController, signup: SignupVM, accountType: String, completion: @escaping (TokenDTO) -> Void) {
     CommonFunctions.post(url: APIClass.register+accountType, parameters: signup.toJSON(), view: view, authRequired: false, completion: completion)
    }
    
    static func addDevice(view: UIViewController, device: AddDeviceVM, completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.post(url: APIClass.addDevice, parameters: device.toJSON(), view: view, authRequired: true, completion: completion)
    }
    
    static func forgotPassword(view: UIViewController, forgotPW:ForgotPassword , completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.post(url: APIClass.forgotPassword, parameters: forgotPW.toJSON(), view: view, authRequired: false, completion: completion)
    }
    
    static func resetPassword(view: UIViewController, resetPW: ResetPasswordVM, completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.post(url: APIClass.resetPassword, parameters: resetPW.toJSON(), view: view, authRequired: false, completion: completion)
        
    }
    
    static func commuterPackages(view: UIViewController, packageType: String, rideType: String, cityOne: String, cityTwo: String, completion: @escaping ([PackagesDTO]) -> Void) {
        CommonFunctions.getArray(url: APIClass.commuterPackages+"/"+packageType+"/"+rideType+"/"+cityOne+"/"+cityTwo+"/all", parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func myPackages(view: UIViewController, packageType: String, rideType: String, cityOne: String, cityTwo: String, completion: @escaping ([PackagesDTO]) -> Void) {
        CommonFunctions.getArray(url: APIClass.commuterPackages+"/"+packageType+"/"+rideType+"/"+cityOne+"/"+cityTwo+"/all", parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func clientPackages(view: UIViewController, packageType: String, rideType: String, cityOne: String, cityTwo: String, riderCount: String, completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.get(url: APIClass.clientPackages+packageType+"/"+rideType+"/"+cityOne+"/"+cityTwo+"/"+riderCount+"/all", view: view, authRequired: true, completion: completion)
    }
    
    static func getPackages(view: UIViewController, id: String, completion: @escaping (PackagesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.getPackages + id, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func myPackages(view: UIViewController, clientPackageStatus: String, page: String, size: String, completion: @escaping (MyPackagesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.myPackages+clientPackageStatus+"/"+"paged?"+page+"/"+size, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    static func activeRides(view: UIViewController, page: String, size: String, completion: @escaping (ActiveRidesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.activeTrips+"paged?"+page+"/"+size, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func ridesPackages(view: UIViewController, page: String, size: String, completion: @escaping (ActiveRidesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.riderPackages+"paged?"+page+"/"+size, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    static func sharedPackgeByMe(view: UIViewController, completion: @escaping (SharedPackagesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.sharedByMe, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    static func singleRideDetail(view: UIViewController, id: String, completion: @escaping (RiderPackageDTO) -> Void){
        CommonFunctions.get(url: APIClass.riderDetail+id, parameters: [:], view: view, authRequired: true, completion: completion)
        
    }
    
    static func sharedPackgeWithMe(view: UIViewController, completion: @escaping (SharedPackagesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.sharedWithMe, parameters: [:], view: view, authRequired: true, completion: completion)
    }

    static func sharePackge(view: UIViewController, completion: @escaping (SharedPackagesDTO) -> Void) {
        CommonFunctions.get(url: APIClass.sharedWithMe, parameters: [:], view: view, authRequired: true, completion: completion)
    }

    
    static func getPackage(view: UIViewController, forgotPW:ForgotPassword, packageType: String, rideType: String, cityOne: String, cityTwo: String, completion: @escaping (TokenDTO) -> Void) {
        CommonFunctions.get(url: APIClass.clientPackages+packageType+"/"+rideType+"/"+cityOne+"/"+cityTwo+"/all", parameters: forgotPW.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func generateToken(view: UIViewController, paymentRes: PaymentVM, completion: @escaping (PaymentResDTO) -> Void) {
        CommonFunctions.post(url: APIClass.generateToken, parameters: paymentRes.toJSON(), view: view, authRequired: true, completion: completion)
    }
    
    static func paymentTransaction(view: UIViewController, pay: PostNounceVM, completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.post(url: APIClass.paymentTransaction, parameters: pay.toJSON(), view: view, authRequired: true, completion: completion)
    }
    
    static func editProfile(view: UIViewController, profile: EditProfileVM, completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.put(url: APIClass.editProfile, parameters: profile.toJSON(), view: view, authRequired: true, completion: completion)
    }
    static func getSearchMember(view: UIViewController,searchText: String, completion: @escaping (GetSearchMemeberList) -> Void) {
        CommonFunctions.get(url: APIClass.searchMember+searchText+"&page=0&size=20", parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func sharePackages(view: UIViewController,share: SharePackageVM , completion: @escaping (ForgotPassDTO) -> Void) {
        CommonFunctions.post(url: APIClass.searchPackages, parameters: share.toJSON(), view: view, authRequired: true, completion: completion)
    }
    
    static func getProfile(view: UIViewController, completion: @escaping (GetProfileDTO) -> Void) {
        CommonFunctions.get(url: APIClass.getProfile, parameters: [:], view: view, authRequired: true, completion: completion)
    }
    
    static func bookTrip(view: UIViewController, bookRide: BookTripVM, completion: @escaping (BookingStatusDTO) -> Void) {
        
        CommonFunctions.post(url: APIClass.bookTrip, parameters: bookRide.toJSON(), view: view, authRequired: true, completion: completion)
        
    }
    static func bookMultipleTrip(view: UIViewController, bookRide: BookMultipleTripVM, completion: @escaping (ForgotPassDTO) -> Void) {
        
        CommonFunctions.post(url: APIClass.bookMultipleTrips, parameters: bookRide.toJSON(), view: view, authRequired: true, completion: completion)
        
    }
    
    static func changePassword(view: UIViewController, changePW: ChangePasswordVM, completion: @escaping (ForgotPassDTO) -> Void) {
        
        CommonFunctions.post(url: APIClass.change_password, parameters: changePW.toJSON(), view: view, authRequired: true, completion: completion)
        
    }
  
}
