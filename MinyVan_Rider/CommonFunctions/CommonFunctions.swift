//
//  CommonFunctions.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection
import Material

class CommonFunctions{
    
    static let minyVan: String = "miny_Van_rider"
    
    static var userToken: String? = nil
    
    static var authHeader: HTTPHeaders = HTTPHeaders();
    
    private static let sessionManager = Alamofire.SessionManager.default
    
    static func loadTokenOnAppLoad() {
        if let token = UserDefaults.standard.string(forKey: minyVan) as String! {
            userToken = token
            authHeader = [
                
                "Authorization": "Bearer " + userToken!
                
            ];
        }
        
        print("User Token : \(String(describing: userToken))")
    }
    
    static func get<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController, authRequired: Bool,completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .get, url: url, parameters: parameters,view: view, authRequired: authRequired, completion: completion)
    }
   
    static func getBaseNtwrk<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController, completion: @escaping (T) -> Void) {
        baseNetworkOperationMethod(method: .get, url: url, parameters: parameters, view: view, completion: completion)
    }
    
    static func getArray<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view : UIViewController, authRequired: Bool, completion: @escaping ([T]) -> Void) {
        baseNetworkOperationArray(method: .get, url: url, parameters: parameters, view: view, authRequired: authRequired, completion: completion)
    }
    
    static func post<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view : UIViewController, authRequired: Bool,completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .post, url: url, parameters: parameters, encoding: JSONEncoding.default,view: view, authRequired: authRequired, completion: completion)
        
    }
    
    static func put<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view : UIViewController,authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .put, url: url, parameters: parameters, encoding: JSONEncoding.default, view: view, authRequired: authRequired, completion: completion)
        
    }
    
    static func delete<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController,authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .delete, url: url, parameters: parameters, view: view, authRequired: authRequired, completion: completion)
        
        
    }
    
    static private func baseNetworkOperation<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default, view : UIViewController, authRequired: Bool, completion: @escaping (T) -> Void) {
//        Sw.startLoading(minimumDisplayTime: 0.3, message: NSLocalizedString("Please wait...", comment: "please wait"))//"Bitte Warten...
        //    Loader.startLoading();
        SwiftLoader.show(animated: true)
        let request:DataRequest;
                if(authRequired) {
                    request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
                } else {
        request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
         }
        print("REQUEST IS : ")
        print(request)
        request.validate()
            .responseObject{ (response: DataResponse<T>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    if(result is TokenDTO) {
                        saveToken(view: view, token: result as! TokenDTO)
                    }
                    print(result)
                    completion(result)
                    //                    view.createAlert(title: "",body: jsonResponse["description"] as! String)
                    //                    if let data = result, let utf8Text = String(data: data, encoding: .utf8) {
                    //                        let json = try? JSONSerialization.jsonObject(with: data)
                    //                        print(json!)
                    //               let jsonResponse = result as? NSObject
                    //                    let resultResponse = jsonResponse["reason"] as! String
                    //                    view.createAlert(title: "", body: "\(jsonResponse.)")
                }
                else {
                    handleError(view: view, response: response)
                }
               SwiftLoader.hide()
                //                Loader.stopLoading()
        }
        
    }
    
    static private func baseNetworkOperationMethod<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default, view : UIViewController,  completion: @escaping (T) -> Void) {
      SwiftLoader.show(animated: true)
        var request:DataRequest;
        //        if(authRequired) {
        request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        //        } else {
        //            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        //        }
        print("REQUEST IS : ")
        print(request)
        request.validate()
            
            .responseObject{ (response: DataResponse<T>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    if(result is TokenDTO) {
                        saveToken(view: view, token: result as! TokenDTO)
                    }
                    print(result)
                    completion(result)
                   
                }
                else {
                    handleError(view: view, response: response)
                }
        SwiftLoader.hide()
        }
        
    }

    static private func baseNetworkOperationArray<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default,  view : UIViewController, authRequired: Bool, completion: @escaping ([T]) -> Void) {
        SwiftLoader.show(animated: true)
//        Loader.startLoading()
        var request:DataRequest;
        if(authRequired) {
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        } else {
            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        }
        print("REQUEST IS : ")
        print(request)
        request.validate()
            .responseArray{ (response: DataResponse<[T]>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    completion(result)
                } else {
                    handleError(view: view, response: response)
                }
              SwiftLoader.hide()
        }
    }
    
    static private func handleError<T>(view: UIViewController, response: DataResponse<T>) {
        
        var statusCode = response.response?.statusCode
        if response.result.isFailure {
            
            //??NETWORK ERROR OR INVALID SERVER RESPONSE??
            
            view.createAlert(title: "OOPS!!", body: "Network error..")
            
        }
        if let error = response.result.error as? AFError {
            statusCode = error._code // statusCode private
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                print("Parameter encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .multipartEncodingFailed(let reason):
                print("Multipart encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .responseValidationFailed(let reason):
                print("Response validation failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    print("Response status code was unacceptable: \(code)")
                    statusCode = code
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    let json = try? JSONSerialization.jsonObject(with: data)
                    print(json ?? "dfghj")
                    
                    if let jsonResponse = json as? NSDictionary {
                        if let errorFields = jsonResponse["fieldErrors"] as? NSArray {
                            if errorFields.count == 0{
                                print("error fields are zero")
                                view.createAlert(title: "Error",body: jsonResponse["description"] as! String)
                                
                            }
                            else{
                                let message = ((errorFields[0] as! NSDictionary)["message"]) as! String
                                
                                print("message......\(message)")
                                view.createAlert(title: "Error", body: "\(message)" )
                            }
                            print("no alert....")
                          // view.createAlert(title: "", body: jsonResponse["description"] as! String)
                        }
                        if jsonResponse["description"] as? String == nil {
                            print("null......")
                        }
                        else{
                            view.createAlert(title: "Error", body: jsonResponse["description"] as! String)
                        }
                    }
                    
                    print("Utf : " + utf8Text)
                    //let errorDTO: ErrorDTO = ErrorDTO(json: utf8Text);
                    //view.createAlert(title: "", body: jsonResponse["description"] as! String)
                }
                
            case .responseSerializationFailed(let reason):
                print("Response serialization failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                view.createAlert(title: "Failed!", body: "Serialization failed")
                // statusCode = 3840 ???? maybe..
            }
            
            print("Underlying error: \(error.underlyingError)")
        } else if let error = response.result.error as? URLError {
            print("URLError occurred: \(error)")
        }
        else {
            print("Unknown error: \(response.result.error)")
        }
        
        print(statusCode ?? "sdfghj")
        //print(statusCode ?? 200) // the status code
        
    }
    
    static private func saveToken(view: UIViewController, token: TokenDTO){
        let preferences = UserDefaults.standard
        
        preferences.setValue(token.id_token, forKey: CommonFunctions.minyVan)
        let didSave = preferences.synchronize()
        
        if !didSave {
            view.createAlert(title: "Error", body: "Unable to save token")
        }
        loadTokenOnAppLoad()
    }
    
    static func logout(view: UIViewController) {
       // SwiftLoader.show(animated: true)
        let preferences = UserDefaults.standard
        
        preferences.setValue(nil, forKey: CommonFunctions.minyVan)
        preferences.synchronize()
        userToken = nil
        authHeader = HTTPHeaders()
       // SwiftLoader.hide()
    }
    
    static let sharedInstance: CommonFunctions = {
        let instance = CommonFunctions()
        // setup code
        return instance
    }()
    private init() { }
    func prepareButton(vc:UIViewController,btn:Button){
        
    }
    
    func appDefaultColor()->UIColor{
        return UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
        
    }
    func appDefaultColorSpinner()->UIColor{
        return UIColor(red: 40/255, green: 160/255, blue: 170/255, alpha: 1)
        
    }
    
    func prepareTextlField(vc:UIViewController,textField:ErrorTextField,placeHolderText:String,detailFieldName:String) {
        // let emailText = ErrorTextField(frame: CGRect(x: 100, y: 100, width: 200, height: 50))
        textField.placeholder = placeHolderText
        textField.detail = detailFieldName//"Error, incorrect email"
        textField.isClearIconButtonEnabled = true
        textField.delegate = vc
        
        let leftView = UIImageView()
      
        switch placeHolderText {
        case "Email":
            leftView.image = Icon.email
        case "Password":
            textField.isSecureTextEntry = true
            leftView.image = UIImage(named:"PasswordLogoLS")
        case "Confirm New Password":
            textField.isSecureTextEntry = true
            leftView.image = UIImage(named:"PasswordLogoLS")
        case "New Password":
            textField.isSecureTextEntry = true
            leftView.image = UIImage(named:"PasswordLogoLS")
        case "Old Password":
            textField.isSecureTextEntry = true
            leftView.image = UIImage(named:"PasswordLogoLS")
        case "Username":
            leftView.image = UIImage(named:"UsernameLogoLS")
        case "Name":
            leftView.image = UIImage(named:"UsernameLogoLS")
        case "Mobile":
            textField.keyboardType = .numberPad
            leftView.image = UIImage(named:"MobileLogoLS")
        case "Enter Otp":
            textField.isSecureTextEntry = true
            leftView.image = UIImage(named:"PasswordLogoLS")
        default:
            print("Error in commonfunction Switch for textfield")
        }
        
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.leftViewNormalColor = .white
        textField.leftViewActiveColor = .white
        textField.textColor = .white
        // textField.dividerContentEdgeInsets = UIEdgeInsetsMake(textField.height - 1,-20 , 0, 0)
        //  Set the colors for the emailField, different from the defaults.
        
        textField.placeholderNormalColor = .white
        textField.placeholderActiveColor = .white
        textField.dividerNormalColor = .white
        textField.dividerActiveColor = .white
        
        //view.addSubview(emailTextField)
    }
    func prepareTextFields(vc:UIViewController,textField:ErrorTextField,placeHolderText:String,detailFieldName:String){
        
        textField.placeholder = placeHolderText
        textField.detail = detailFieldName//"Error, incorrect email"
        textField.isClearIconButtonEnabled = true
        textField.delegate = vc
        textField.placeholderNormalColor = .white
        textField.textColor = .white
        textField.placeholderActiveColor = .white
        textField.dividerNormalColor = .white
        textField.dividerActiveColor = .white
    }
    
}

extension UIViewController: TextFieldDelegate {
    /// Executed when the 'return' key is pressed when using the emailField.
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = true
        return true
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(textField: UITextField, didChange text: String?) {
        print("did change", text ?? "")
    }
    
    public func textField(textField: UITextField, willClear text: String?) {
        print("will clear", text ?? "")
    }
    
    public func textField(textField: UITextField, didClear text: String?) {
        print("did clear", text ?? "")
    }
    
}
