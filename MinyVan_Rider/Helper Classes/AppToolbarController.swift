//
//  AppToolbarController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class AppToolbarController: ToolbarController {
    
    static let appClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    
    fileprivate var menuButton: IconButton!
    fileprivate var switchControl: Switch!
    fileprivate var moreButton: IconButton!
    var editButton: IconButton!
    var backBtn: IconButton!
    
    override func prepare() {
        super.prepare()
        prepareMenuButton()
        prepareStatusBar()
        prepareToolbar()
        
    }
    
}

extension AppToolbarController {
    
    public func prepareMenuButton() {
        menuButton = IconButton(image: Icon.cm.menu, tintColor: .white)
//        editButton = IconButton(image: Icon.cm.add, tintColor: AppToolbarController.blueClr)
//        backBtn = IconButton(image: Icon.cm.arrowBack, tintColor: AppToolbarController.blueClr)
                    menuButton.addTarget(self, action: #selector(handleMenuButton), for: .touchUpInside)
    }
    
    fileprivate func prepareStatusBar() {
        statusBar.backgroundColor = AppToolbarController.appClr
        //Color.appRed
        
        //statusBar.backgroundColor =
    }
    public func itemsOnToolbar(_ sender: Bool){
        if (sender == false){
            toolbar.backgroundColor = AppToolbarController.appClr
            toolbar.titleLabel.textColor = .white//AppToolbarController.
            toolbarController?.toolbar.backgroundColor = AppToolbarController.appClr
            //                toolbar.borderColor = .red
            //                toolbar.borderWidth = 1 //AppToolbarController.grey//UIColor.white //Color.white
            //                toolbar.titleLabel.borderColor = AppToolbarController.lightGrey
            //                toolbar.titleLabel.borderWidth = 1
            
        }
        else{
            toolbar.leftViews = []
        }
    }
    fileprivate func prepareToolbar() {
        
        toolbar.backgroundColor = AppToolbarController.appClr//.white
//        toolbar.shadowColor = UIColor.gray
//        toolbar.layer.shadowOffset.width = 1//shadowOffset.width = 1
//        toolbar.layer.shadowOffset.height = 2
//        toolbar.layer.shadowRadius = 1
//        toolbar.layer
//            .shadowOpacity = 0.5
        //AppToolbarController.redClr
        //            toolbar.dividerColor = AppToolbarController.redColor
        //            //Color.clear
        //            toolbar.dividerThickness = 1
        // toolbar.plainView.backgroundColor = AppToolbarController.redColor
        
        toolbar.titleLabel.textColor = .white//AppToolbarController.appClr
        
        //  toolbar.
        //            toolbar.titleLabel.textColor = UIColor.red
        //toolbar.titleLabel.font = UIFont.appFont
//         toolbar.leftViews = [menuButton]
        
    }
    
    func toggleMenus(left: Bool, right: Bool) {
        setLeftMenuItem(left)
        setRightMenuItem(right)
    }
    
    
    func setLeftMenuItem(_ show: Bool) {
        if show {
            toolbar.leftViews = [menuButton]
        }
        else {
            toolbar.leftViews = []
        }
    }
    
    
    func setRightMenuItem(_ show: Bool) {
        if show {
            
            toolbar.rightViews = []
            
        } else {
            toolbar.rightViews = []
        }
    }
    
}

extension AppToolbarController {
    @objc
    fileprivate func handleMenuButton() {
        navigationDrawerController?.toggleLeftView()
    }
}

