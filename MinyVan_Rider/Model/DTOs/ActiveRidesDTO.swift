//
//  ActiveRidesDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class ActiveRidesDTO: EVObject{

    var content = Array<RiderPackageDTO>()
    
}
