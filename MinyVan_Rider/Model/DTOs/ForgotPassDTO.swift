//
//  ForgotPassDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class ForgotPassDTO: EVObject{
    
    var reason: String = ""
    
}
