//
//  GetProfileDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class GetProfileDTO: EVObject{
    
    var password : String?
    var name = ""
    var username = ""
    var email = ""
    var mobile = ""
    var activated = false
}
