//
//  MyPackagesDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class MyPackagesDTO: EVObject{
    
    var content = Array<MyPackagesResDTO>()
    var totalElements =  0
    var totalPages =  0
    var last =  false
    var sort :  String?
    var numberOfElements =  0
    var first =  false
    var size =  0
    var number =  0
    
}
