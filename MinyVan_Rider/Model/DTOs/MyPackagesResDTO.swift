//
//  MyPackagesResDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class MyPackagesResDTO: EVObject{

       var  clientPackageId  =  0
       var  name  =  ""
       var  tipAmount  =  0
       var  totalRides  =  0
       var  usedRides  =  0
       var  activationDate  =  0.0
       var  expiryDate  =  0.0
       var  packageType  = ""
       var  rideType  =  ""
       var  owner  =  ""
       var  minyVanPackageId  =  0
       var  paymentId  =  0
       var  packagePrice  =  0
       var  sharedBy  :  String?
       var  sharedByMobile  :  String?
       var  sharedByEmail  :  String?

}
