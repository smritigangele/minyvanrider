//
//  packagesDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class PackagesDTO: EVObject{
    
    var packageId = 0
    var name = ""
    var packageValidity = 0
    var numberOfRides = 0
    var amount = 0
    var packageType = ""
    var rideType = ""
    var routeId = 0
    
}
