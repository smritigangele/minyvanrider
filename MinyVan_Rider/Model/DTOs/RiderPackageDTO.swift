//
//  RiderPackageDTO.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class RiderPackageDTO: EVObject{
    
   var tripId =  0
    var phoneNumber : String?
   var bookingTime =  0.0
   var plannedPickUpTime =  0.0
   var plannedDropOffTime =  0.0
    var rideStartTime : String?
    var rideEndTime : String?
   var bookingStatus = ""
   var tripStatus = ""
    var tip : String?
   var numberOfRiders =  0
   var driverRating =  0
   var riderRating =  0
   var rider = ""
   var driver = ""
   var pickUpLat = 0.0
   var pickUpLong = 0.0
   var dropOffLat = 0.0
   var dropOffLong = 0.0
   var clientPackage = 0
   var packageName = ""
   var route =  0
   var routeName = ""
   var source = ""
   var destination = ""
  
}

