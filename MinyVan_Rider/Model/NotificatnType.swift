//
//  NotificatnType.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 26/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

enum NotificatnType{
   
   case DRIVER_ALLOTTED
   case DRIVER_30_MIN
   case DRIVER_15_MIN
   case DRIVER_5_MIN
   case RIDE_STARTED
   case RIDE_ENDED
    
}
