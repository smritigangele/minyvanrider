//
//  AddDeviceVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 20/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class AddDeviceVM {
    
    var  deviceID  =  ""
    var deviceName  =  ""
    var deviceType  = ""
    var messagingID  = ""
    var ownerId  = ""
    var id  = ""
    
    public func toJSON() -> Dictionary<String, String> {
        return [
            
            "deviceID": self.deviceID,
            "deviceName": self.deviceName,
            "deviceType": self.deviceType,
            "messagingID": self.messagingID,
            "ownerId": self.ownerId
        ]
    }
}
