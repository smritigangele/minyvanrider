//
//  BookMultipleTripVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

class BookMultipleTripVM{
    
   var dates = [String]()
   var tripBookingStatus = ""
   var tripId =  0
  
    func toJSON() -> Dictionary<String, Any>{
        var parameters = Dictionary<String, Any>()
        parameters = [
            "tripId": "\(self.tripId)",
            "tripBookingStatus": self.tripBookingStatus
        ]
        if dates.count != 0{
            parameters["dates"] = self.dates
        }
//        if tripBookingStatus != ""{
//            parameters["tripBookingStatus"] = self.tripBookingStatus
//        }
        return parameters
    }
}

