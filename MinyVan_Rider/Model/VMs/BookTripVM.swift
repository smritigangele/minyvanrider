//
//  BookTripVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class BookTripVM: EVObject{

   var destination = ""
   var dropOffLat = 0.0
   var dropOffLong = 0.0
   var numberOfRiders = 0
   var packageType = "COMMUTER"
   var pickUpLat = 0.0
   var pickUpLong = 0.0
   var plannedDropOffTime = ""
   var plannedPickUpTime = ""
   var rideType = "RIDE_SHARE"
   var routeId = 0
   var source = ""
    
    func toJSON() -> Dictionary<String, String>{
        
       return[
        
            "destination": self.destination,
            "dropOffLat": "\(self.dropOffLat)",
            "dropOffLong": "\(self.dropOffLong)",
            "numberOfRiders": "\(self.numberOfRiders)",
            "packageType": self.packageType,
            "pickUpLat": "\(self.pickUpLat)",
            "pickUpLong": "\(self.pickUpLong)",
            "plannedDropOffTime": self.plannedDropOffTime,
            "plannedPickUpTime": self.plannedPickUpTime,
            "rideType": self.rideType,
            "routeId": "\(self.routeId)",
            "source": self.source
        
        ]
        
    }
    
}
