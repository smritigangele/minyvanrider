//
//  ChangePasswordVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

class ChangePasswordVM{
    
    var oldPassword = ""
    var newPassword = ""
    
    func toJSON() -> Dictionary<String, String>{
        
        return[
        
                "oldPassword": self.oldPassword,
                "newPassword": self.newPassword
            
           ]
        
      }
}
