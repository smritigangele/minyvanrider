//
//  EditProfileVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class EditProfileVM: EVObject{
    
    var email = ""
    var mobile = ""
    var name = ""
    
    func toJSON() -> Dictionary<String, String>{
        
        return[
        
                "email": self.email,
                "mobile": self.mobile,
                "name": self.name

        ]
    }
}
