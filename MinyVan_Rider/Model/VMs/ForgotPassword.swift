//
//  ForgotPassword.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

class ForgotPassword{
    
    var mobile: String = ""
    
    func toJSON() -> Dictionary<String, String>{
        
        return[
            "mobile": self.mobile
        ]
        
    }
    
}
