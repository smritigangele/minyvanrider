//
//  LoginVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class LoginVM{
    
    var username: String = ""
    var password: String = ""
    var loginType: String = "NORMAL"
    
    public func toJSON() -> Dictionary<String, Any> {
       return[
        "username": self.username,
        "password": self.password,
        "loginType": self.loginType
        
        ]
    }
    
}
