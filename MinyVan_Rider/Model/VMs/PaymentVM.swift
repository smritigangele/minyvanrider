//
//  PaymentVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
class PaymentVM{

    var amount = ""
    var minyVanPackageId = ""
    var tipAmount = ""

    func toJSON()-> Dictionary<String, String>{
        
        return[
            
                "amount": self.amount,
                "minyVanPackageId": self.minyVanPackageId,
                "tipAmount": self.tipAmount
            
        ]
    }
}
