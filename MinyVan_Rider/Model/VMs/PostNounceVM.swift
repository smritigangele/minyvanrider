//
//  PostNounceVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 14/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

class PostNounceVM{
   
    var nonce = ""
    var paymentId = ""
    
    
    func toJSON()-> Dictionary<String, String>{
        
     return[
    
         "nonce": self.nonce,
         "paymentId": self.paymentId
    
        ]
    
    }
}
