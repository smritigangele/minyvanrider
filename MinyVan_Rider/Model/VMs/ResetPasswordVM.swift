//
//  ResetPasswordVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class ResetPasswordVM: EVObject{

    var key = ""
    var newPassword = ""

    
    func toJSON() -> Dictionary<String, String>{
        
        return[
        
                "key": self.key,
                "newPassword": self.newPassword
        
        ]
    }
}
