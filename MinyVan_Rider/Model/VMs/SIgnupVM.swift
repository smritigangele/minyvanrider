//
//  SIgnupVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class SignupVM{
    
    var email: String = ""
    var mobile: String = ""
    var name: String = ""
    var password: String = ""
    var username: String = ""
 
    func toJSON() -> Dictionary<String, String>{
        return[
            
            "email": self.email,
            "mobile": self.mobile,
            "name": self.name,
            "password": self.password,
            "username": self.username
        ]
    }
    
}
