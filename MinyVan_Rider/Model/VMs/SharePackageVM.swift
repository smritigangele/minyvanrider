//
//  SharePackageVM.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import EVReflection

class SharePackageVM: EVObject{

   var clientPackageId = 0
   var ridesToShare = 0
   var username = ""
    
    func toJSON() -> Dictionary<String, String>{
        
        return[
                "clientPackageId": "\(self.clientPackageId)",
                "ridesToShare": "\(self.ridesToShare)",
                "username": self.username
        ]
    }
}
