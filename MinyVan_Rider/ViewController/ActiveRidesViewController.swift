//
//  ActiveRidesViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class ActiveRidesViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
  
    var data = ActiveRidesDTO()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        self.getURL()
    }
    
    func getURL(){
        
        APIManger.activeRides(view: self, page: "0", size: "20"){
            (completion: ActiveRidesDTO) in
            print(completion)
            self.data = completion
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
   
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        
    self.dismiss(animated: true, completion: nil)
        
    }
    
}

extension ActiveRidesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActiveRidesCell
        let date = Date(timeIntervalSince1970: self.data.content[indexPath.row].bookingTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        cell.dateLbl.text! = strDate
        cell.sourceLbl.text! = self.data.content[indexPath.row].source
        cell.destinationLbl.text! = self.data.content[indexPath.row].destination
        cell.priceLbl.text! = "$ " +  "\(self.data.content[indexPath.row].clientPackage)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 105
    }
    
}
