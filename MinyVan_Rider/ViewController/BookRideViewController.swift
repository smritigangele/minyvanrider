//
//  BookRideViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 08/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material
import Alamofire
import Braintree
//import JTAppleCalendar

class BookRideViewController: UIViewController,BTDropInViewControllerDelegate{
    
    let selectedClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    @IBOutlet var heightConstraintForGoForPayment: NSLayoutConstraint!
    
    @IBOutlet var bottomConstraintGoForPAyment: NSLayoutConstraint!
    
    @IBOutlet var viewForConfirmDate: UIView!
    @IBOutlet var viewForDateSelector: UIView!
    @IBOutlet var heightForDateSelectorView: NSLayoutConstraint!
    @IBOutlet var chooseDropOffLabel: UILabel!
      var blurEffectViewS = UIVisualEffectView()
    var isFromResultGreaterThanOne = false
    var braintree: Braintree?
    var id: Int!
    @IBOutlet var noOfRidersTextField: TextField!
    @IBOutlet var noOfRidersView: UIView!
    @IBOutlet var pickupTimeTextFiled: UITextField!
    @IBOutlet var pickUpDateTextFiled: UITextField!
    
    @IBOutlet var goForPaymentBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet var fifDBtn: Button!
    @IBOutlet var tenDBtn: Button!
    @IBOutlet var fiveDBtn:Button!
    @IBOutlet var twoDBtn:Button!
    @IBOutlet var oneDBtn:Button!
    var amountText:String!
    var totalDaysText:String!
    var totalTripText:String!
    var choosePickUpText:String!
    var chooseDropText:String!
    var typeText:String!
    var packageID:String!
    var string: String!
    var newDate = ""
    var text1: String? = nil
    var hideStatusBar = false
    var dateTimeStamp:Double!
    var timeTimeStamp:Double!
    var result:Double!
    var finalDate: Date!
    let formatter = DateFormatter()
    var datesToBeSelected: [String] = []
    var bookingStatus = BookingStatusDTO()
    @IBOutlet var currentWeekBtn: UIButton!
    @IBOutlet var monthBtn: UIButton!
    @IBOutlet var selecteDateBtn: UIButton!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var goForPayment: UIButton!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var pickUpTimeLabel: UIView!
    @IBOutlet var pickUpDateLabel: UIView!
    @IBOutlet var totalDays: UILabel!
    @IBOutlet var totalTripLabel: UILabel!
    @IBOutlet var ammontLabel: UILabel!
    @IBOutlet var choosePickUPLabel: UILabel!
    @IBOutlet var upperBGView: UIView!
    var paymentID:String!
    var rideType:String!
    var numberOfRiders:String!
    var destinationLat:String!
    var destinationLng:String!
    var sourceLat:String!
    var sourceLng:String!
    var sourceString:String!
    var detinationString:String!
    var routeID:String!
     let gesture = UITapGestureRecognizer()
    static let kOverlayHeight: CGFloat = 80;
     let shadowClr = UIColor(red: 194/255, green: 195/255, blue: 199/255, alpha: 1)
     let grayClr = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
  
    var rangeSelectedDates: [Date] = []
    var testCalendar = Calendar.current
    //var prePostVisibility: ((CellState, CustomCell?)->())?
    var currentWeekCheckBox = false {
        didSet {
            if (currentWeekCheckBox == true) {
                
                currentWeekBtn.backgroundColor = selectedClr
                 monthBtn.backgroundColor = .white
                  selecteDateBtn.backgroundColor = .white
                
            }
            else {
                
                currentWeekBtn.backgroundColor = .white
            }
            
        }
        
    }
    var monthCheckBox = false {
        didSet {
            
            if (monthCheckBox == true) {
                
                monthBtn.backgroundColor = selectedClr
                currentWeekBtn.backgroundColor = .white
                  selecteDateBtn.backgroundColor = .white
            }
            else {
                
                monthBtn.backgroundColor = .white
            }
            
        }
        
    }
    var selectedDateCheckBox = false{
        
        didSet{
            
            if (selectedDateCheckBox == true) {
                selecteDateBtn.backgroundColor = selectedClr
                monthBtn.backgroundColor = .white
                currentWeekBtn.backgroundColor = .white
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
                vc.tripId = self.bookingStatus.tripId
                vc.modalTransitionStyle = .coverVertical
                vc.modalPresentationStyle = .overFullScreen
                blurEffectViewS.removeFromSuperview()
                (viewForConfirmDate).removeFromSuperview()
                //vc.modalPresentationCapturesStatusBarAppearance = true
                self.present(vc,animated: true,completion: nil)
                
            }
                
         else {
                
               selecteDateBtn.backgroundColor = .white
           }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        noOfRidersTextField.keyboardType = .numberPad
        noOfRidersTextField.placeholder = "Enter Amount"
        noOfRidersTextField.isClearIconButtonEnabled = true
        noOfRidersTextField.placeholderNormalColor = .lightGray
        noOfRidersTextField.placeholderActiveColor = .lightGray
        noOfRidersTextField.dividerNormalColor = .black
        noOfRidersTextField.dividerActiveColor = .black
        
        totalAmountLabel.text = amountText
        upperBGView.layer.shadowColor = shadowClr.cgColor
        upperBGView.layer.shadowOffset = CGSize(width: 0, height: 3)
        upperBGView.layer.shadowOpacity = 0.5
        upperBGView.layer.masksToBounds = false
        
        pickUpDateLabel.layer.borderWidth = 0.5
        pickUpDateLabel.layer.borderColor = UIColor.darkGray.cgColor
        pickUpTimeLabel.layer.borderWidth = 0.5
        pickUpTimeLabel.layer.borderColor = UIColor.darkGray.cgColor
        
        fifDBtn.addTarget(self, action:  #selector(BookRideViewController.btnPressed), for: .touchUpInside)
        tenDBtn.addTarget(self, action:  #selector(BookRideViewController.btnPressed), for: .touchUpInside)
        fiveDBtn.addTarget(self, action:  #selector(BookRideViewController.btnPressed), for: .touchUpInside)
        twoDBtn.addTarget(self, action:  #selector(BookRideViewController.btnPressed), for: .touchUpInside)
        oneDBtn.addTarget(self, action:  #selector(BookRideViewController.btnPressed), for: .touchUpInside)
        
        goForPayment.layer.cornerRadius = 2
        goForPayment.clipsToBounds = true
        
        typeLabel.text = typeText
        ammontLabel.text = amountText
        chooseDropOffLabel.text = chooseDropText
        choosePickUPLabel.text = choosePickUpText
        print("total trip label:- \(totalTripText)")
        totalTripLabel.text = totalTripText
        totalDays.text = totalDaysText
         print("total trip label:- \(totalDaysText)")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
       
        if isFromResultGreaterThanOne {
            for subView in  view.subviews.filter({$0.tag > 70}){
                subView.isHidden = true
            }
            goForPaymentBtnWidthConstraint.constant = UIScreen.main.bounds.width - 10
            heightConstraintForGoForPayment.constant = 42
            bottomConstraintGoForPAyment.constant = 3
            goForPayment.setTitle("Book Ride", for: .normal)
        }

    }

    @IBAction func didTapOnBackBtn(_ sender: Any) {
        
     self.dismiss(animated: true, completion: nil)
        
    }

    @objc func btnPressed(_ sender:Button) {
        
        sender.titleColor = CommonFunctions.sharedInstance.appDefaultColor()
        
        let subTotal = String(amountText.characters.dropFirst())
        
        for index in 77...81 {
            let btn = self.view.subviews.filter({$0.tag == index}).first! as! Button
            if btn != sender {
                btn.titleColor = UIColor.darkGray
            }else{
                switch index {
                case 77:
                    totalAmountLabel.text =  "$" + String(Int(subTotal)! + 1)
                    break
                case 78:
                    totalAmountLabel.text =  "$" + String(Int(subTotal)! + 2)
                    break
                case 79:
                    totalAmountLabel.text =  "$" + String(Int(subTotal)! + 5)
                    break
                case 80:
                    totalAmountLabel.text =  "$" + String(Int(subTotal)! + 10)
                    break
                case 81:
                    totalAmountLabel.text =  "$" + String(Int(subTotal)! + 15)
                    break
                default:
                    print("ohhhoooooo error in switch...")
                }
            }
        }
    }
   
    @IBAction func pickUpTimeAction(_ sender: Any) {
        pickupTimeTextFiled.becomeFirstResponder()
    }
    
    @IBAction func pickUpDateAction(_ sender: Any) {
        pickUpDateTextFiled.becomeFirstResponder()
    }
    
    
    @IBAction func pickUpDateTextFieldAction(_ sender: Any) {
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(BookRideViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatterNew = DateFormatter()
        dateFormatterNew.dateStyle = DateFormatter.Style.short
        dateFormatterNew.dateFormat = "yyyy-MM-dd" //"EEE MMM dd HH:mm:ss 'GMT'Z yyyy"
        let newDate = dateFormatterNew.string(from: sender.date)
        print("new date:- \(newDate)")
        let currentDate = dateFormatterNew.string(from: Date())
        print("current date:- \(currentDate)")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateTimeStamp = sender.date.timeIntervalSince1970
        print("date time stamp :- \(dateTimeStamp)")
        pickUpDateTextFiled.text = newDate //dateFormatter.string(from: sender.date)
        
    }
    
    func convertTimeAndDate(date:String,time: String){
        
        let date = pickUpDateTextFiled.text
        let time = pickupTimeTextFiled.text
        
        var dateString = date! + " " + time!
        var df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        var date1: Date? = df.date(from: dateString)
        var since1970: TimeInterval? = date1?.timeIntervalSince1970
        result = since1970! * 1000
        print("result is :-\(result!)")
    }
    
    @objc func timePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .none
        
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = "hh:mm:ss"
        timeTimeStamp = sender.date.timeIntervalSince1970
        pickupTimeTextFiled.text = dateFormatter.string(from: sender.date)
        
    }
    @IBAction func pickUptimeTextFieldAction(_ sender: Any) {
        
        if pickUpDateTextFiled.text == "Pickup Date" {

            self.createAlert(title: "Alert!", body: "Please select date")
            return
        }
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.time
        datePickerView.backgroundColor = grayClr
        datePickerView.layer.masksToBounds = false
        datePickerView.layer.shadowColor = UIColor.black.cgColor
        datePickerView.layer.shadowOpacity = 0.5
        datePickerView.layer.shadowOffset = CGSize(width: -1, height: 1)
        datePickerView.layer.shadowRadius = 1
        
        datePickerView.layer.shadowPath = UIBezierPath(rect: datePickerView.bounds).cgPath
        datePickerView.layer.shouldRasterize = true
        
        datePickerView.setValue(UIColor.black, forKeyPath: "textColor")
        (sender as! UITextField).inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(BookRideViewController.timePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    // MARK:- to make payment for the ride
    
    @IBAction func didTabForPayment(_ sender: Any) {
        
        if pickUpDateTextFiled.text == "Pickup Date" {
          self.createAlert(title: "", body: "Please select date")
            return
        }
        
        if pickupTimeTextFiled.text == "Pickup Time" {
          self.createAlert(title: "", body: "Please select time")
            return
        }
        
        let tip = Int(String(totalAmountLabel.text!.characters.dropFirst()))! - Int(String(amountText.characters.dropFirst()))!
        
        if !isFromResultGreaterThanOne {
           
            let payment = PaymentVM()
            payment.amount = String(amountText.characters.dropFirst())
            payment.minyVanPackageId = self.packageID
            payment.tipAmount = "\(tip)"
            APIManger.generateToken(view: self, paymentRes: payment){
                (completion: PaymentResDTO) in
                 print(completion)
                
                    self.paymentID = completion.purchaseId
                    self.braintree = Braintree(clientToken: completion.paymentToken)
                    // Create a BTDropInViewController
                    let dropInViewController = self.braintree!.dropInViewController(with: self)
                    // This is where you might want to customize your Drop-in. (See below.)
                    // The way you present your BTDropInViewController instance is up to you.
                    // In this example, we wrap it in a new, modally presented navigation controller:
                    dropInViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(BookRideViewController.userDidCancelPayment))

                    let navigationController = UINavigationController(rootViewController: dropInViewController)
                    self.present(navigationController, animated: true, completion: nil)
                
                 }
           }
        else{
            
            self.openAlertForConfirmDate()
            
        }
        
    }

    func userDidCancelPayment() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func drop(inViewControllerDidCancel viewController: BTDropInViewController!) {
        // Send payment method nonce to your server
        dismiss(animated: true, completion: nil)
    }
    func drop(_ viewController: BTDropInViewController!, didSucceedWith paymentMethod: BTPaymentMethod!) {
        postNonceToServer(paymentMethodNonce: paymentMethod.nonce)
        dismiss(animated: true, completion: nil)

    }
    
    func postNonceToServer(paymentMethodNonce: String) {

        let postNonce = PostNounceVM()
        postNonce.nonce = paymentMethodNonce
        postNonce.paymentId = self.paymentID
        
        SwiftLoader.show(animated: true)
        APIManger.paymentTransaction(view: self, pay: postNonce){
            (completion: ForgotPassDTO) in
        self.createAlert(title: "", body: "Payment is done sucessfully!!")

            SwiftLoader.hide()
        
           self.bookTrip()
       }
        
    }
    
    @IBAction func actionOnCurrentWeekBtn(_ sender: Any) {
       self.currentWeekCheckBox = !self.currentWeekCheckBox
        if currentWeekCheckBox == true{
             bookMultipleTrip()
        }
    }
  
    @IBAction func actionOnMonthBtn(_ sender: Any) {
        
        self.monthCheckBox = !self.monthCheckBox
        if monthCheckBox == true{
           bookMultipleTrip()
        }
   
    }

    @IBAction func actionOnSelectDateBtn(_ sender: Any) {
     
       self.selectedDateCheckBox = !self.selectedDateCheckBox

    }
    
    //MARK:- book mutiple trips (MONTHLY, WEEKLY or CUSTOM)
    
    func bookMultipleTrip(){
        
      let bookMultipleTrip = BookMultipleTripVM()
        
        if monthCheckBox == true{
           bookMultipleTrip.tripBookingStatus = "MONTHLY"
            blurEffectViewS.removeFromSuperview()
            (viewForConfirmDate).removeFromSuperview()
        }
        
        if currentWeekCheckBox == true{
            
           bookMultipleTrip.tripBookingStatus = "WEEKLY"
            blurEffectViewS.removeFromSuperview()
            (viewForConfirmDate).removeFromSuperview()
    
        }

         bookMultipleTrip.tripId = self.bookingStatus.tripId
        APIManger.bookMultipleTrip(view: self, bookRide: bookMultipleTrip){
            (completion: ForgotPassDTO) in
            print(completion)
            self.createAlert(title: "Booking Completed", body: completion.reason)
            self.blurEffectViewS.removeFromSuperview()
            (self.viewForConfirmDate).removeFromSuperview()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK:- Open Calendar to Select dates
    
    @IBAction func actionOnCancelBtn(_ sender: Any) {
        blurEffectViewS.removeFromSuperview()
       (viewForConfirmDate).removeFromSuperview()
        
    }
    
    // MARK:- To book multiple trip
    
    func openAlertForConfirmDate(){
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        blurEffectView.alpha = 0.5
        self.blurEffectViewS = blurEffectView
        gesture.addTarget(self, action: #selector(dissmissB(_:)))
        // gesture.delegate = self
        blurEffectView.isUserInteractionEnabled = true
        blurEffectView.addGestureRecognizer(gesture)
        self.view.addSubview(blurEffectView)
        self.view.bringSubview(toFront:blurEffectView)
        viewForConfirmDate.layer.cornerRadius = 5
        viewForConfirmDate.frame = CGRect(x: (UIScreen.main.bounds.width - self.viewForConfirmDate.width)/2, y: 200, width: viewForConfirmDate.frame.width, height: viewForConfirmDate.frame.height)
        viewForConfirmDate.clipsToBounds = true
        self.view.addSubview(self.viewForConfirmDate)
        self.view.bringSubview(toFront:self.viewForConfirmDate)

        self.bookTrip()
    }
    
    func dissmissB(_ tab:UITapGestureRecognizer){
       // textFieldForPrice.text = ""
        tab.view?.removeFromSuperview()
        (viewForConfirmDate).removeFromSuperview()
    }
    // MARK:- Trip Booking
    
    func bookTrip() {
        
        convertTimeAndDate(date: pickUpDateTextFiled.text!, time: pickupTimeTextFiled.text!)
      
        if let lastUpdated : String = "\(result)" {
            
            let epocTime = TimeInterval(result) / 1000
            let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            dateFormatter.date(from: String(describing: unixTimestamp))
            let updatedTimeStamp = unixTimestamp
            newDate = dateFormatter.string(from: updatedTimeStamp as Date)
            print("updated time as date:- \(newDate)")
            
        }
        
        let bookTrip = BookTripVM()
        bookTrip.rideType = rideType
        bookTrip.packageType = "COMMUTER"
        bookTrip.destination = self.detinationString
        bookTrip.dropOffLat = Double(self.destinationLat)!
        bookTrip.dropOffLong = Double(self.destinationLng)!
        bookTrip.numberOfRiders = Int(self.numberOfRiders)!
        bookTrip.pickUpLat = Double(self.sourceLat)!
        bookTrip.pickUpLong = Double(self.sourceLng)!
        bookTrip.source = self.sourceString
        bookTrip.plannedPickUpTime = newDate
        bookTrip.plannedDropOffTime = newDate
        bookTrip.routeId = Int(routeID)!
        print(bookTrip)
        APIManger.bookTrip(view: self, bookRide: bookTrip){
            (completion: BookingStatusDTO) in
            print(completion)
            self.bookingStatus = completion
            //let alertController = UIAlertController(title: "Alert", message: "Payment Complete", preferredStyle: .alert)

            // Create the actions
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
//                            UIAlertAction in
//                            NSLog("OK Pressed")
//
//             }
            // Add the actions
           // alertController.addAction(okAction)
            // Present the controller
          //  self.present(alertController, animated: true, completion: nil)
            }
    }
    
    //MARK:- To give tip other than shown numbers for a ride.
    
    @IBAction func othersBtnAction(_ sender: Any) {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        blurEffectView.alpha = 0.5
        self.blurEffectViewS = blurEffectView
        self.view.addSubview(blurEffectView)
        self.view.bringSubview(toFront:blurEffectView )
        noOfRidersView.frame = CGRect(x: (UIScreen.main.bounds.width - noOfRidersView.frame.width)/2, y: 200, width: noOfRidersView.frame.width, height: noOfRidersView.frame.height)
        noOfRidersView.layer.cornerRadius = 5
        noOfRidersView.clipsToBounds = true
        noOfRidersTextField.isUserInteractionEnabled = true
        
        self.view.addSubview(self.noOfRidersView)
        self.view.bringSubview(toFront:self.noOfRidersView )
        
    }
    
    @IBAction func doneBtnAction(_ sender: Any) {
        if  noOfRidersTextField.text?.trimmingCharacters(in: ["0"]) == "" {
            
           self.createAlert(title: "", body: "Please enter a valid amount")
            return
            
        }
        let subTotal = String(amountText.characters.dropFirst())
        
        totalAmountLabel.text =  "$" + String(Int(subTotal)! + Int(noOfRidersTextField.text!)!)
        
        blurEffectViewS.removeFromSuperview()
        noOfRidersView.removeFromSuperview()
        
        noOfRidersTextField.text = ""
        
    }
    override func dismissKeyboard() {
        view.endEditing(true)
    }
}
    
    
        




