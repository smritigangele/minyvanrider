//
//  CalendarViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit
import JTAppleCalendar

class CalendarViewController: UIViewController, JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    //let formatter = DateFormatter()
    var tripId: Int!
    var newDate = ""
    var selectedDates = [String]()
    var dateStrings = [String]()
        let todaysDate = Date()
        let todaysDateColor = UIColor.white
        let selectedDateColor = UIColor.white
        let thisMonthColor = UIColor.black
        let notThisMonthColor = UIColor.lightGray
        var headerArrowNextDate = Date()
        let userCalendar =  Calendar.current
    
    @IBOutlet weak var dateLbl: UILabel!
    //weak var delegate: ViewControllerBDelegate?
    
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet var weekdayLbl: UILabel!
        @IBOutlet weak var calendarView: JTAppleCalendarView!
        @IBOutlet weak var headerYear: UILabel!
        @IBOutlet weak var headerMonth: UILabel!
    
    var rangeSelectedDates: [Date] = []
    
    func didStartRangeSelecting(cellState: CellState, cell: JTAppleCell?, date: Date) {
        
        rangeSelectedDates = calendarView.selectedDates
        
//        if !rangeSelectedDates.contains(date){
//
            let dateRange = calendarView.generateDateRange(from: rangeSelectedDates.first ?? date, to: date)
            for aDate in dateRange {
                //  if !rangeSelectedDates.contains(aDate) {
                //  rangeSelectedDates.append(aDate)
                // }
            }//rangeSelectedDates.first!
            calendarView.selectDates([rangeSelectedDates.first!], triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: true)
        
//        } else {
//
//            let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)! + 1
//            let lastIndex = rangeSelectedDates.endIndex
//            rangeSelectedDates.removeSubrange(indexOfNewlySelectedDate..<lastIndex)
//        }
        
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = "yyyy-MM-dd"
        
        for date in rangeSelectedDates {
            let now = dateformatter.string(from: date)
            dateStrings.append(now)
        }
        print(rangeSelectedDates)
    }
    func getDay(text: Date){
        
        formatter.string(from: text)
        let day = Calendar.current.component(.day, from: text)
        let year = Calendar.current.component(.year, from: text)
        let monthLbl = Calendar.current.component(.month, from: text)
        self.dateLbl.text! = "\(day)"
        let dayofWeek1 = text.dayOfWeek()!
        print("month :- \(monthLbl)")
        self.weekdayLbl.text! = dayofWeek1
        self.yearLbl.text! = String(year)
        
        print(formatter.string(from: text))
        print(Date().dayOfWeek()!)
        
//        formatter.dateFormat = "MMMM"
//        let nameOfMonth = formatter.string(from: text)
//        self.month.text! = formatter.string(from: text)
//        self.formatter.dateFormat = "yyyy"
//        self.year.text! = self.formatter.string(from: text)
        
    }
        func setupCalendarView() {
            calendarView.minimumLineSpacing = 0
            calendarView.minimumInteritemSpacing = 0
        }
        
        func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy MM dd"
            formatter.timeZone = Calendar.current.timeZone
            formatter.locale =  Calendar.current.locale
            
            let startDate = userCalendar.date(byAdding: Calendar.Component.year, value: -50, to: Date())   // date(byAdding: addForBusinessDays, to: Date())//formatter.date(from: "2017 01 31")! //Date()
            let endDate = userCalendar.date(byAdding: Calendar.Component.year, value: 50, to: Date())//formatter.date(from: "2017 12 31")!
            let parameters = ConfigurationParameters(startDate: startDate!,
                                                     endDate: endDate!,
                                                     numberOfRows: 5, // Only 1, 2, 3, & 6 are allowed
                calendar: Calendar.current,
                generateInDates: .forAllMonths,
                generateOutDates: .tillEndOfRow,
                firstDayOfWeek: .sunday)
            return parameters
            
        }
        
        func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
            guard let validCell = view as? CustomCell else { return }
            formatter.dateFormat = "yyyy MM dd"
            let todaysDateString = formatter.string(from: todaysDate)
            let monthDateString = formatter.string(from: cellState.date)
            // print("cellState \(cellState.isSelected)")
            
            if todaysDateString == monthDateString {
                validCell.dateLbl.textColor = .black
                
            }
            else {
                if cellState.isSelected {
                    validCell.selectedView.layer.cornerRadius = 16
                    validCell.selectedView.clipsToBounds = true
                    validCell.dateLbl.textColor = selectedDateColor
                    
                }
                else {
                    
                    if cellState.dateBelongsTo == .thisMonth {
                        validCell.dateLbl.textColor = thisMonthColor
                        if cellState.date.isWeekend == true{
                            validCell.dateLbl.textColor = notThisMonthColor
                        }
                        
                    }
                    else {
                        validCell.dateLbl.textColor = notThisMonthColor
                    }
                }
            }
         
        }
        
        
        func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
            guard let validCell = view as? CustomCell else { return }
            if cellState.isSelected {
                if cellState.date.isWeekend == true{
                    validCell.dateLbl.textColor = notThisMonthColor
                    validCell.selectedView.isHidden = true
                }
                else{
                    validCell.selectedView.isHidden = false
                    print("Valid Cell selected")
                }
                //            validCell.selectedView.isHidden = false
                
            } else {
                validCell.selectedView.isHidden = true
                // print("Invalid Cell ")
            }
            
        }
        
        func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
            let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath        ) as! CustomCell
            cell.dateLbl.text = cellState.text
            handleCellSelected(view: cell, cellState: cellState)
            handleCellTextColor(view: cell, cellState: cellState)
            return cell
        }
        func handleCellConfiguration(cell: JTAppleCell?, cellState: CellState) {
            handleCellSelected(view: cell, cellState: cellState)
            handleCellTextColor(view: cell, cellState: cellState)
            
        }
        func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
            getDay(text: date)
            if date.isWeekend == true{
                self.createAlert(title: "OOPS!", body: "You cann't book ride on weekends")
            }
            else{
                
                didStartRangeSelecting(cellState: cellState, cell: cell, date: date)
                handleCellSelected(view: cell, cellState: cellState)
                handleCellTextColor(view: cell, cellState: cellState)
             
            }
        }
        
        func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
            
            if rangeSelectedDates.count == 0{
               handleCellSelected(view: cell, cellState: cellState)
               handleCellTextColor(view: cell, cellState: cellState)
            }
            else{
                let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)!
                print(indexOfNewlySelectedDate)
                rangeSelectedDates.remove(at: indexOfNewlySelectedDate)
                print(rangeSelectedDates)
                handleCellSelected(view: cell, cellState: cellState)
                handleCellTextColor(view: cell, cellState: cellState)
            }
//            handleCellSelected(view: cell, cellState: cellState)
//            handleCellTextColor(view: cell, cellState: cellState)
        }
        func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
            let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "proRateHeader", for: indexPath) as! calendarHeaderClass
            return header
        }
        
        func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
            return MonthSize(defaultSize: 50)
        }
        
        // This sets the height of your header
        func calendar(_ calendar: JTAppleCalendarView, sectionHeaderSizeFor range: (start: Date, end: Date), belongingTo month: Int) -> CGSize {
            return CGSize(width: 200, height: 50)
        }
        // This setups the display of your header
        func calendar(_ calendar: JTAppleCalendarView, willDisplaySectionHeader header: JTAppleCollectionReusableView, range: (start: Date, end: Date), identifier: String) {
            let headerCell = (header as? calendarHeaderClass)
            headerCell?.month.text = "Hello Header"
        }
        
        func calendarDidScroll(_ calendar: JTAppleCalendarView) {
            return
            
        }
        
        
        func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
            
            let date = visibleDates.monthDates.first!.date
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            headerYear.text = formatter.string(from: date)
            
            formatter.dateFormat = "MMM"
            headerMonth.text = formatter.string(from: date)
            // calendarView.selectDates([Date()])
            
        }
        
        let formatter: DateFormatter = {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy MM dd"
            dateFormatter.timeZone = Calendar.current.timeZone
            dateFormatter.locale =  Calendar.current.locale
            return dateFormatter
        }()
        
        
        func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
            let myCustomCell = cell as! CustomCell
            
            // Setup Cell text
            myCustomCell.dateLbl.text = cellState.text
            
            // Setup text color
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dateLbl.textColor = thisMonthColor
            } else {
                myCustomCell.dateLbl.textColor = notThisMonthColor
            }
        }
    
        @IBAction func minusYearBtn(_ sender: UIButton) {
            let next = userCalendar.date(byAdding: .year, value: -1, to: calendarView.selectedDates[0])!
            calendarView.scrollToDate(next, animateScroll: false)
            calendarView.deselectAllDates()
            calendarView.selectDates([next], triggerSelectionDelegate: true)
        }
        
        @IBAction func plusYearBtn(_ sender: UIButton) {
            let next = userCalendar.date(byAdding: .year, value: 1, to: calendarView.selectedDates[0])!
            calendarView.scrollToDate(next, animateScroll: false)
            calendarView.deselectAllDates()
            calendarView.selectDates([next], triggerSelectionDelegate: true)
            
        }
        
        @IBAction func minusMonthBtn(_ sender: UIButton) {
            let next = userCalendar.date(byAdding: .month, value: -1, to: calendarView.selectedDates[0])!
            calendarView.scrollToDate(next, animateScroll: false)
            calendarView.selectDates([next], triggerSelectionDelegate: true)
        }
        
        @IBAction func plusMonthBtn(_ sender: UIButton) {
            let next = userCalendar.date(byAdding: .month, value: 1, to: calendarView.selectedDates[0])!
            calendarView.selectDates([next], triggerSelectionDelegate: true)
            calendarView.scrollToDate(next, animateScroll: false)
        }
        
        override func viewDidLoad() {
            setupCalendarView()
            calendarView.allowsMultipleSelection = true
            calendarView.scrollToDate(Date())
            calendarView.selectDates([Date()])
            
             let date = Date()
             getDay(text: date)
            
        }
    
    
    @IBAction func actionOnCancel(_ sender: Any) {
        rangeSelectedDates.removeAll()
        selectedDates.removeAll()
        dateStrings.removeAll()
        print("range :- \(rangeSelectedDates)")
        print("selected dates array :- \(selectedDates)")
        print("date string array :- \(dateStrings)")
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionOnOK(_ sender: Any) {
        
        print("range of dates ;- \(dateStrings)")
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        for date in rangeSelectedDates{
            //let date1: Date? = df.date(from: date)
            let since1970: TimeInterval? = date.timeIntervalSince1970
            print("result is :-\(since1970!)")
            let someValue: Float = Float(since1970!)
            let finalValue = someValue.cleanValue
            //let longValue = Int(since1970)
            newDate = "\(finalValue)"
            selectedDates.append(newDate)
        }
        print("final string; - \(selectedDates)")
        
        if self.selectedDates.count == 0{
            self.createAlert(title: "Alert!", body: "Please select the date")
        }
        let bookMultipleTrip = BookMultipleTripVM()
        bookMultipleTrip.tripId = tripId
        bookMultipleTrip.dates = self.selectedDates
        bookMultipleTrip.tripBookingStatus = "CUSTOM"
        
        APIManger.bookMultipleTrip(view: self, bookRide: bookMultipleTrip){
            (completion: ForgotPassDTO) in
            print(completion)
            self.createAlert(title: "Booking Completed", body: completion.reason)
           // let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "BookRideViewController") as! BookRideViewController
//            let presentingVC = self.presentingViewController
            self.selectedDates.removeAll()
            self.dateStrings.removeAll()
            self.rangeSelectedDates.removeAll()
            self.dismiss(animated: false, completion: { () -> Void   in
                self.dismiss(animated: true, completion: nil)
            })
          //  presentingVC!.present(destinationController, animated: true, completion: nil)
           
          
           // self.navigationController?.popToRootViewController(animated: true)
            
        }
        
        
    }
    
    }


