//
//  ChangePasswordViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material
import Alamofire

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet var backBtn: Button!
    @IBOutlet var nameTextField: ErrorTextField!
    @IBOutlet var mobileNumnberTextField: ErrorTextField!
    @IBOutlet var emailTextField: ErrorTextField!
    @IBOutlet var scroolView: UIScrollView!
    @IBOutlet var widthOfScrollView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        let icon = Icon.arrowBack
        backBtn.setImage(icon?.tint(with: .white), for:.normal)
        emailTextField.delegate = self
        mobileNumnberTextField.delegate = self
        nameTextField.delegate = self
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:emailTextField ,placeHolderText:"Confirm New Password",detailFieldName:"Error, incorrect Password")
        
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:mobileNumnberTextField ,placeHolderText:"New Password",detailFieldName:"Error, incorrect Password")
        
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:nameTextField,placeHolderText:"Old Password",detailFieldName:"Error, incorrect Password")
        widthOfScrollView.constant = UIScreen.main.bounds.width - 1
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @IBAction func didTapOnBackBtn(_ sender: Any) {
      dismiss(animated: true, completion: nil)
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            let scrPt = CGPoint(x: 0, y: textField.frame.origin.y/4)
            self.scroolView!.setContentOffset(scrPt, animated: true)
        }
    }
    override func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations:{ self.scroolView!.setContentOffset(CGPoint.zero, animated: true)})
        }
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapOnChangePassword(_ sender: Any) {
        
        if nameTextField.text == "" {
            nameTextField.detail = "Enter Old Password"
            nameTextField.isErrorRevealed = true
            self.createAlert(title: "Alert!!", body: "Enter Old Password")

            return
        }
        
        if mobileNumnberTextField.text == "" {
            mobileNumnberTextField.detail = "Enter New Password"
            mobileNumnberTextField.isErrorRevealed = true
           self.createAlert(title: "Alert!!", body: "Enter New Password")
            return
            
        }
        
        if emailTextField.text == "" {
            emailTextField.detail = "Enter Confirm New Password"
            emailTextField.isErrorRevealed = true
             self.createAlert(title: "Alert!!", body: "Enter Confirm New Password")
            return
        }
        
        if emailTextField.text != mobileNumnberTextField.text {
            
           self.createAlert(title: "Alert!!", body: "Confirm Password and New Password Should be the same")
            
            return
            
        }
        
        changePassword()
   }
    
    func changePassword(){
        
        let changePW = ChangePasswordVM()
        changePW.oldPassword = self.nameTextField.text!
        changePW.newPassword = self.mobileNumnberTextField.text!
        
        APIManger.changePassword(view: self, changePW: changePW){
            (completion: ForgotPassDTO) in
            print(completion)
            
            let loginController: LoginViewController = {
                return UIStoryboard.viewController(identifier: "LoginViewController") as! LoginViewController
            }()
            
            newWindow!.rootViewController = loginController
            
            newWindow!.makeKeyAndVisible()
            //UIApplication.shared.statusBarStyle = .lightContent
            for window in UIApplication.shared.windows {
                if window.isKeyWindow != true {
                    
                    window.removeFromSuperview()
                }
           }
       }
    }
}
