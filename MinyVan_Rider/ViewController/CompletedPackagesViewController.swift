//
//  CompletedPackagesViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class CompletedPackagesViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var data = MyPackagesDTO()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      tableView.delegate = self
        tableView.dataSource = self
        getData()
    }
    
    func getData(){
        
        APIManger.myPackages(view: self, clientPackageStatus: "COMPLETED", page: "0", size: "20"){
            (completion: MyPackagesDTO) in
            self.data = completion
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    private func preparePageTabBarItem() {
        
        pageTabBarItem.title = "Complete"
        pageTabBarItem.titleColor = .white
        pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
    }
    
}

    extension CompletedPackagesViewController: UITableViewDelegate,UITableViewDataSource{
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.data.content.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyPackagesViewCell
            
//                    cell.viewForList.layer.cornerRadius = 4
//                    cell.viewForList.clipsToBounds = true
//                    cell.viewForList.layer.shadowOffset.width = 2
//                    cell.viewForList.layer.shadowOffset.height = 1
//            
//                    cell.viewForList.layer.shadowColor = UIColor.lightGray.cgColor
//            
//                   cell.viewForList.layer.shadowOpacity = 0.7
//                   cell.viewForList.layer.shadowRadius = 5
            
                   cell.pkgNameForCompletion.text = self.data.content[indexPath.row].name
                   cell.leftTripsForCompletion.text = "\(self.data.content[indexPath.row].totalRides)"
                   cell.priceForCompletion.text = "$ " + "\(self.data.content[indexPath.row].packagePrice)"
            let date = Date(timeIntervalSince1970: self.data.content[indexPath.row].expiryDate)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateStyle = .medium
            dateFormatter.dateFormat = "dd-MM-yyyy" //Specify your format that you want
            let strDate = dateFormatter.string(from: date)
            cell.expiryDateForCompletion.text = strDate
            
            return cell
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 105
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            print("hahahahaha you can not go forward")
        }

}
