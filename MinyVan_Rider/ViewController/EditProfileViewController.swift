//
//  EditPasswordViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material
import Alamofire

class EditProfileViewController: UIViewController{
    
    
    @IBOutlet var backBtn: Button!
    @IBOutlet var nameTextField: ErrorTextField!
    @IBOutlet var mobileNumnberTextField: ErrorTextField!
    @IBOutlet var emailTextField: ErrorTextField!
    
    @IBOutlet var scroolView: UIScrollView!
    @IBOutlet var widthOfScrollView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        let icon = Icon.arrowBack
        backBtn.setImage(icon?.tint(with: .white), for:.normal)
        emailTextField.delegate = self
        mobileNumnberTextField.delegate = self
        nameTextField.delegate = self
        
        CommonFunctions.sharedInstance.prepareTextFields(vc:self,textField:emailTextField ,placeHolderText:"Email",detailFieldName:"Error, incorrect Username")
        CommonFunctions.sharedInstance.prepareTextFields(vc:self,textField:mobileNumnberTextField ,placeHolderText:"Mobile",detailFieldName:"Error, incorrect Mobile Number")
        CommonFunctions.sharedInstance.prepareTextFields(vc:self,textField:nameTextField,placeHolderText:"Name",detailFieldName:"Error, incorrect Name")

        widthOfScrollView.constant = UIScreen.main.bounds.width - 1
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        get_User_Data()
    }
    
    func get_User_Data(){
        
        APIManger.getProfile(view: self){
            (completion: GetProfileDTO) in
            print(completion)
            
            self.emailTextField.text! = completion.email
            self.nameTextField.text! = completion.name
            self.mobileNumnberTextField.text! = completion.mobile
            
        }
        
    }
    
   
    override func dismissKeyboard() {
      
        view.endEditing(true)
    }
    
    @IBAction func didTapOnBackBtn(_ sender: Any) {
   
        self.dismiss(animated: true, completion: nil)
        
    }
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            let scrPt = CGPoint(x: 0, y: textField.frame.origin.y/4)
            self.scroolView!.setContentOffset(scrPt, animated: true)
        }
    }
    override func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations:{ self.scroolView!.setContentOffset(CGPoint.zero, animated: true)})
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func didTapNExtBtn(_ sender: Any) {
        if emailTextField.text == "" {
            emailTextField.detail = "Enter Email"
            emailTextField.isErrorRevealed = true
            
            return
        }
        if mobileNumnberTextField.text == "" {
            emailTextField.detail = "Enter Mobile Number"
            emailTextField.isErrorRevealed = true
            
            return
        }
        if nameTextField.text == "" {
            emailTextField.detail = "Enter Mobile Name"
            emailTextField.isErrorRevealed = true
            
            
            return
        }
        
        editProfile()
  }
    
    func editProfile(){
     
        let profile = EditProfileVM()
        profile.email = emailTextField.text!
        profile.mobile = mobileNumnberTextField.text!
        profile.name = nameTextField.text!
        
        APIManger.editProfile(view: self, profile: profile){
            (completion: ForgotPassDTO) in
            print(completion)
        
        }
    }
    
}
