//
//  ExpiryPackagesViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class ExpiryPackagesViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    var data = MyPackagesDTO()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      tableView.delegate = self
      tableView.dataSource = self
        
      self.getData()
        
    }
    
    func getData(){
        
        APIManger.myPackages(view: self, clientPackageStatus: "EXPIRED", page: "0", size: "20"){
            (completion: MyPackagesDTO) in
            self.data = completion
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    private func preparePageTabBarItem() {
        
        pageTabBarItem.title = "Expired"
        pageTabBarItem.titleColor = .white
        pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
    }
    
}
    extension ExpiryPackagesViewController: UITableViewDelegate,UITableViewDataSource{
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.data.content.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyPackagesViewCell
            
            cell.pkgNameForExpired.text = self.data.content[indexPath.row].packageType
            cell.leftTripsForExpired.text = "\(self.data.content[indexPath.row].totalRides)"
            cell.priceLblForExpired.text = "$ " + "\(self.data.content[indexPath.row].packagePrice)"
            let date = Date(timeIntervalSince1970: self.data.content[indexPath.row].expiryDate)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateStyle = .medium
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: date)
            cell.dateForExpiredPkg.text = strDate
            
            return cell
            
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            
            return 105
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            print("ohhnnnoooo, so sad your trip expired..!!")
            
        }
}
