//
//  ForgetPasswordViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var mobileTextFiled: ErrorTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:mobileTextFiled ,placeHolderText:"Mobile",detailFieldName:"Error, incorrect Moblie Number")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        mobileTextFiled.keyboardType = .numberPad
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func didTapBackBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func didTapNextBtn(_ sender: Any) {
        if mobileTextFiled.text!.trimmingCharacters(in: .whitespaces) == "" {
            mobileTextFiled.isErrorRevealed = true
            return
        }
        forgetPassWord()
    }
    
    func forgetPassWord(){
        
        let forgotPW = ForgotPassword()
        forgotPW.mobile = mobileTextFiled.text!
        
        APIManger.forgotPassword(view: self, forgotPW: forgotPW){
            (completion: ForgotPassDTO) in
            
           // self.createAlert(title: "", body: completion.reason)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    
    }
        
}

