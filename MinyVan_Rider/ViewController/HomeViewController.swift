//
//  HomeViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material
import GoogleMaps
import GooglePlaces

class HomeViewController: UIViewController,GMSMapViewDelegate {

    let shadowClr = UIColor(red: 194/255, green: 195/255, blue: 199/255, alpha: 1)
    
    var isBookingdone = false
    @IBOutlet var tableViewForCommuterPkgs: UITableView!
    var blurEffectViewS = UIVisualEffectView()
    var blurEffectViewSideMenu:UIVisualEffectView?
    @IBOutlet var rideTypeView: UIView!
     @IBOutlet weak var noOfRidersTextField: TextField!
    @IBOutlet weak var rideShare: Button!
    @IBOutlet weak var doneBtn: Button!
    @IBOutlet weak var charterBtn: Button!
    var resultViewController:GMSAutocompleteViewController?
    @IBOutlet var noOfRidersView: UIView!
    @IBOutlet weak var btnCharter: Button!
    @IBOutlet weak var noOfRidersLabel: UILabel!
    @IBOutlet weak var rideTypeLabel: UILabel!
    @IBOutlet weak var rideShareBtn: Button!
    var locationManager = CLLocationManager()
    @IBOutlet var lowerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var scrollViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var googleMapHeightConstraint: NSLayoutConstraint!
    @IBOutlet var googleMap: GMSMapView!
    
    @IBOutlet var endLocation: UILabel!
    
    @IBOutlet var startLocation: UILabel!
    
    @IBOutlet var locationBackground: UIView!
    
    var destination:GMSPlace?
    var origin:GMSPlace?
    var desText:String?
    var orText:String?
    var isPickUpSelceted = true
    var packagesList = PackagesDTO()
    var packages = [PackagesDTO]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let icon = Icon.menu
   self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //sideMenuBtn.setImage(icon?.tint(with: .white), for: .normal)
        noOfRidersTextField.keyboardType = .numberPad
        prepareToolbar()
       //preparePageTabBarItem()
       // self.navigationDrawerController?.isLeftViewEnabled = true
        locationBackground.layer.shadowColor = shadowClr.cgColor
        locationBackground.layer.shadowOffset = CGSize(width: 0, height: 3)
        locationBackground.layer.shadowOpacity = 0.7
        locationBackground.layer.masksToBounds = false
        locationManager.delegate = self
       // googleMap.camera =  GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        
        locationBackground.frame = CGRect(x: 16, y: 21, width: UIScreen.main.bounds.width - 32, height: locationBackground.frame.size.height)
      
        lowerViewHeightConstraint.constant =  UIScreen.main.bounds.height - 235
//        scrollViewWidthConstraint.constant = UIScreen.main.bounds.width
        googleMapHeightConstraint.constant = UIScreen.main.bounds.height - 116
        self.view.addSubview(locationBackground)
        self.view.bringSubview(toFront: locationBackground)
        //self.view.addSubview(googleMap)
        scrollView.delegate = self
        self.scrollView.isScrollEnabled = false
        //self.scrollView.isDirectionalLockEnabled = true
        
        noOfRidersTextField.placeholder = "No of riders"
        // noOfRidersTextField.detail = detailFieldName//"Error, incorrect email"
        noOfRidersTextField.isClearIconButtonEnabled = true
        noOfRidersTextField.delegate = self
        noOfRidersTextField.placeholderNormalColor = .lightGray
        noOfRidersTextField.placeholderActiveColor = .lightGray
        noOfRidersTextField.dividerNormalColor = .black
        noOfRidersTextField.dividerActiveColor = .black
        tableViewForCommuterPkgs.delegate = self
        tableViewForCommuterPkgs.dataSource = self
        googleMap.delegate = self
        googleMap.isMyLocationEnabled = true
        
       // tableView.reloadData()
        
    }

    
    fileprivate func prepareToolbar() {
        
        guard let tc = toolbarController as! AppToolbarController! else {
            return
        }
        tc.prepareMenuButton()
        tc.toolbar.title = "Commuter Rides"
        tc.toolbar.titleLabel.textColor = .white//UIColor.red
        tc.toolbar.detail = ""
        
        tc.toggleMenus(left: true, right: true)
        
    }
    
//    func  preparePageTabBarItem(){
//        pageTabBarItem.titleLabel?.font = UIFont.appTextFont
//
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isBookingdone {
            desText = nil
            orText = nil
            destination = nil
            origin  = nil
            rideTypeLabel.text = "Ride Type"
            noOfRidersLabel.text = "No of Riders"
            endLocation.text = "Choose drop-off location"
            startLocation.text = "Choose pick-up location"
            googleMap.clear()
            scrollView.setContentOffset(CGPoint(x:0,y:0), animated: false)
            tableViewForCommuterPkgs.reloadData()
            self.scrollView.isScrollEnabled = false
            isBookingdone  = false
        }
        
    }
    
    @IBAction func rideTypeActionBtn(_ sender: Any) {
        
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        blurEffectView.alpha = 0.5
        self.blurEffectViewS = blurEffectView
        self.view.addSubview(blurEffectView)
        self.view.bringSubview(toFront:blurEffectView )

        rideTypeView.frame = CGRect(x: (UIScreen.main.bounds.width - rideTypeView.frame.width)/2, y: 200, width: rideTypeView.frame.width, height: rideTypeView.frame.height)
        rideTypeView.layer.cornerRadius = 5
        rideTypeView.clipsToBounds = true


        self.view.addSubview(self.rideTypeView)
        self.view.bringSubview(toFront:self.rideTypeView )
       //  blurEffectView.insertSubview(self.noOfRidersView, at: 2)
//        self.googleMap.addSubview(self.rideTypeView)
       //  getValuesForOriginAndDesString()
    }
    
    @IBAction func noOfRiders(_ sender: Any) {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        blurEffectView.alpha = 0.5
        self.blurEffectViewS = blurEffectView
        
        self.view.addSubview(blurEffectView)
        self.view.bringSubview(toFront:blurEffectView )
        noOfRidersView.frame = CGRect(x: (UIScreen.main.bounds.width - noOfRidersView.frame.width)/2, y: 200, width: noOfRidersView.frame.width, height: noOfRidersView.frame.height)
        noOfRidersView.layer.cornerRadius = 5
        noOfRidersView.clipsToBounds = true
        noOfRidersTextField.isUserInteractionEnabled = true
        self.view.addSubview(self.noOfRidersView)
        self.view.bringSubview(toFront:self.noOfRidersView )
        
        // self.googleMap.addSubview(self.noOfRidersView)
      // blurEffectView.insertSubview(self.noOfRidersView, at: 2)
         //getValuesForOriginAndDesString()
        
    }
    
    @IBAction func endLocationBtnAction(_ sender: Any) {
        isPickUpSelceted = false
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    @IBAction func startLocationBtnAction(_ sender: Any) {
        isPickUpSelceted = true
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
    }
    
    @IBAction func chaterBtnAction(_ sender: Any) {
        
        blurEffectViewS.removeFromSuperview()
        (rideTypeView).removeFromSuperview()
//        blurEffectViewS.alpha = 0
//        rideTypeView.alpha = 0
        rideTypeLabel.text = "CHARTER"
        getValuesForOriginAndDesString()

    }
    

    @IBAction func rideShareAction(_ sender: Any) {
        blurEffectViewS.removeFromSuperview()
        (rideTypeView).removeFromSuperview()
//        blurEffectViewS.alpha = 0
//        rideTypeView.alpha = 0
        rideTypeLabel.text = "RIDE-SHARE"
        getValuesForOriginAndDesString()
    }
    
    
    @IBAction func doneBtnAction(_ sender: Any) {

        if  noOfRidersTextField.text?.trimmingCharacters(in: ["0"]) == "" {

            self.createAlert(title: "", body: "Enter number of riders")
//            CommonFunctions.sharedInstance.showAlertWithSimpleMsg(viewController: self, title: "Alert", msg: "Enter number of riders")
            return


        }

        if  Int(noOfRidersTextField.text!)! >= 6 {
            
        self.createAlert(title: "", body: "Number of riders should not be more than 5")
//            CommonFunctions.sharedInstance.showAlertWithSimpleMsg(viewController: self, title: "Alert", msg: "Number of riders should not be more than 5")
            return
        }

        blurEffectViewS.removeFromSuperview()
        noOfRidersView.removeFromSuperview()
        noOfRidersLabel.text = noOfRidersTextField.text! + " Riders"
        noOfRidersTextField.text = ""
       
         getValuesForOriginAndDesString()
    }
    
    func getCityString(start:Double,end:Double,type:Int){
        var destinationText = ""
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(CLLocationCoordinate2DMake(start, end)) { response, error in
            if let addressObj = response?.firstResult() {
                print("locality=\(addressObj.locality)")
                if addressObj.locality != nil {
                    destinationText =  destinationText + (addressObj.locality! as String)
                }
                print("administrativeArea=\(addressObj.administrativeArea)")
                if addressObj.administrativeArea != nil {
                    destinationText =  destinationText + (addressObj.administrativeArea! as String)
                }
                print("postalCode=\(addressObj.postalCode)")
                print("country=\(addressObj.country)")
                if addressObj.country != nil {
                    destinationText =  destinationText + (addressObj.country! as String)
                }
                if addressObj.postalCode != nil {
                    destinationText =  destinationText + (addressObj.postalCode! as String)
                }
                print("lines=\(addressObj.lines)")
                if type == 0 {
                    self.orText = destinationText
                }else{
                    self.desText = destinationText
                }
                if self.orText != nil && self.desText != nil {
                    self.toGetPackages()
                }
            }
        }
    }
    
    func toGetPackages(){
        
        SwiftLoader.show(animated: true)
        var rideShareText = ""
        
        if rideTypeLabel.text == "RIDE-SHARE" {
            
            rideShareText = "RIDE_SHARE"
        }else{
            rideShareText = "CHARTER"
            
        }
        
        let numberOfRidersText = noOfRidersLabel.text?.characters.first!
        
       //numberOfRidersText.replacingOccurrences(of: " ", with: "%20")
        
        APIManger.clientPackages(view: self, packageType: "COMMUTER", rideType: rideShareText, cityOne: orText!.replacingOccurrences(of: " ", with: "%20"), cityTwo: desText!.replacingOccurrences(of: " ", with: "%20"),riderCount: "\(numberOfRidersText!)"){
            
            (completion: ForgotPassDTO) in
            print(completion)

            SwiftLoader.hide()
          
            if Int(completion.reason) != -1 {
               
                let alertController = UIAlertController(title: "Alert", message: "Already have a package for this route.", preferredStyle: .alert)
                
                
                // Create the actions
                let okAction = UIAlertAction(title: "Book Ride", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.toGetFinalPackageIfResultIsGreaterThanOne(id:(completion.reason))
                    NSLog("Book Ride Pressed")
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
            }
                 else{
                     self.toGetFinalPackage()
                        }
                }
        }
    
    func toGetFinalPackage(){
       
        var rideShareText = ""
        
        if rideTypeLabel.text == "RIDE-SHARE" {
            
            rideShareText = "RIDE_SHARE"
        }
        else{
            rideShareText = "CHARTER"
        }
        
        APIManger.commuterPackages(view: self, packageType: "COMMUTER", rideType: rideShareText, cityOne: orText!.replacingOccurrences(of: " ", with: "%20"), cityTwo: desText!.replacingOccurrences(of: " ", with: "%20")){
            (completion: [PackagesDTO]) in
            print(completion)
            self.packages = completion
          self.scrollView.setContentOffset(CGPoint(x:0,y: self.scrollView.contentSize.height - UIScreen.main.bounds.height + 40), animated: true)

          self.scrollView.isScrollEnabled = true
          self.tableViewForCommuterPkgs.reloadData()
            
        }
        
    }

      func toGetFinalPackageIfResultIsGreaterThanOne(id:String!){
        
            var rideShareText = ""
            
            if rideTypeLabel.text == "RIDE-SHARE" {
                
                rideShareText = "RIDE_SHARE"
            }else{
                rideShareText = "CHARTER"
                
            }
        
        APIManger.getPackages(view: self, id: id){
            (completion: PackagesDTO) in
            print(completion)
            self.packagesList = completion
        
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookRideViewController") as! BookRideViewController
            vc.amountText  = "$"  + "\(self.packagesList.amount)"
            vc.typeText = self.packagesList.packageType
            vc.choosePickUpText = self.origin?.formattedAddress
            vc.sourceString = self.origin?.formattedAddress
            vc.sourceLng = String(((self.origin?.coordinate.longitude)! as Double))
            vc.sourceLat = String(((self.origin?.coordinate.latitude)! as Double))
            vc.chooseDropText = self.destination?.formattedAddress
            vc.detinationString = self.destination?.formattedAddress
            vc.destinationLng = String(((self.destination?.coordinate.longitude)! as Double))
            vc.destinationLat = String(((self.destination?.coordinate.latitude)! as Double))
            vc.totalDaysText = "\(self.packagesList.packageValidity)"
            print("package days:- \(self.packagesList.packageValidity)")
            vc.totalTripText = "\(self.packagesList.numberOfRides)"
            print("package rides:- \(self.packagesList.numberOfRides)")
            vc.packageID = "\(self.packagesList.packageId)"
            vc.routeID = "\(self.packagesList.routeId)"
            vc.isFromResultGreaterThanOne = true
            if self.rideTypeLabel.text == "RIDE-SHARE" {
                vc.rideType = "RIDE_SHARE"
                }
                else{
                vc.rideType = "CHARTER"
                }
                            
            vc.numberOfRiders = String(describing: self.noOfRidersLabel.text?.characters.first! as AnyObject)
           self.present(vc, animated: true, completion: nil)
                            
            }
        }
    
    @IBAction func didTapupDownArrow(_ sender: Any) {
        
        if self.startLocation.text! == "Choose pick-up location" && self.endLocation.text! == "Choose drop-off location"{
            self.createAlert(title: "", body: "Please enter your pick up and drop location")
        }
            
        else{
            
        UIView.animate(withDuration: 0.4, animations: {
            (sender as! UIButton).transform = (sender as! UIButton).transform.rotated(by: CGFloat(2*M_PI_2))
            //  self.instagramTimeCapButton.transform = CGAffineTransformMakeRotation(CGFloat(M_PI*2))
        }, completion: { (finished: Bool) -> Void in
            
            var temp:GMSPlace!
            temp = self.origin
            self.origin = self.destination
            self.destination = temp
            var tempText:String!
            tempText = self.startLocation.text!
            self.startLocation.text = self.endLocation.text!
            self.endLocation.text = tempText
            if self.origin == nil {
                self.startLocation.text = "Choose pick-up location"
            }
            if self.destination == nil {
                self.endLocation.text = "Choose drop-off location"
            }
            if self.origin != nil &&  self.destination != nil {
                
                self.getValuesForOriginAndDesString()
            }
        })
        
     }
  }

}

extension HomeViewController: CLLocationManagerDelegate{
    static let zoom = 15
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
           
            googleMap.camera = GMSCameraPosition(target: location.coordinate, zoom: Float(HomeViewController.zoom), bearing:0, viewingAngle:0)
               let newMarker = GMSMarker()
               newMarker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "startLocation"), scaledToSize: CGSize(width: 60, height: 60))
               newMarker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
               newMarker.map = self.googleMap
               newMarker.map = nil
            locationManager.stopUpdatingLocation()
            if let mylocation = googleMap.myLocation {
                print("User's location: \(mylocation)")
            } else {
                print("User's location is unknown")
            }
        }
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        print("image of marker:- \(image)  &  \(newSize)")
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            googleMap.isMyLocationEnabled = true
            googleMap.settings.myLocationButton = true
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("Yes there is some error:- \(error.localizedDescription)")
    }
    
}
extension HomeViewController: GMSAutocompleteViewControllerDelegate,UIScrollViewDelegate{
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        dismiss(animated: true, completion: nil)
        
        let camera = GMSCameraPosition.camera(withLatitude:place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15)
        //self.googleMap.myLocationEnabled = true
    
        self.googleMap.camera = camera
        
        if isPickUpSelceted {
            startLocation.text = place.formattedAddress
            origin = place
            print("origin:- \(origin)")
            let marker = GMSMarker()
            marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "startLocation"), scaledToSize: CGSize(width: 60, height: 60))
            marker.position = CLLocationCoordinate2DMake(place.coordinate.latitude,place.coordinate.longitude)
            marker.title = place.formattedAddress
            //marker.snippet = place.
            marker.map = self.googleMap
            if origin != nil && destination != nil {
                getRoute()
            }
        }else{
            endLocation.text = place.formattedAddress
            let marker = GMSMarker()
            //marker.icon = #imageLiteral(resourceName: "endLocation")
            marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "endLocation"), scaledToSize: CGSize(width: 60, height: 60))
            marker.position = CLLocationCoordinate2DMake(place.coordinate.latitude,place.coordinate.longitude)
            marker.title = place.formattedAddress
            //marker.snippet = place.
            marker.map = self.googleMap
            destination = place
            if origin != nil && destination != nil {
                getRoute()
            }
        }
        getValuesForOriginAndDesString()
    }
    
    func getRoute(){
        
        googleMap.clear()
        
        let markerO = GMSMarker()
        markerO.icon = #imageLiteral(resourceName: "startLocation")
        markerO.position = CLLocationCoordinate2DMake(origin!.coordinate.latitude,origin!.coordinate.longitude)
        markerO.title = origin!.name
        //marker.snippet = place.
        markerO.map = self.googleMap
        
        let markerD = GMSMarker()
        markerD.icon = #imageLiteral(resourceName: "endLocation")
        markerD.position = CLLocationCoordinate2DMake(destination!.coordinate.latitude,destination!.coordinate.longitude)
        markerD.title = destination!.name
        //marker.snippet = place.
        markerD.map = self.googleMap
        var bounds = GMSCoordinateBounds()
        
        var request = URLRequest(url: URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=place_id:\(origin!.placeID)&destination=place_id:\(destination!.placeID)&key=AIzaSyDpVCjWYDmjZMFC4QqhtV8izZpM7jC9zUc")!)
        request.httpMethod = "GET"
        let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print("json:",json)
                
                
                if let routes = json["routes"] as? [[String : AnyObject]] {
                    if routes.count > 0 {
                        var first = routes.first
                        if let legs = first!["legs"] as? [[String : AnyObject]] {
                            // let fullPath : GMSPolyline = GMSPolyline()
                            for leg in legs {
                                if let steps = leg["steps"] as? [[String : AnyObject]] {
                                    for step in steps {
                                        if let polyline = step["polyline"] as? [String : AnyObject] {
                                            if let points = polyline["points"] as? String {
                                                //                                      fullPath.path = GMSMutablePath(fromEncodedPath: points)
                                                DispatchQueue.main.sync {
                                                    let path: GMSPath = GMSPath(fromEncodedPath: points)!
                                                    let routePolyline = GMSPolyline(path: path)
                                                    routePolyline.strokeColor = .black
                                                    routePolyline.strokeWidth = 4
                                                    routePolyline.map = self.googleMap
                                                    
                                                    print(path.count())
                                                    for index in 1...path.count() {
                                                        bounds = bounds.includingCoordinate(path.coordinate(at: index))
                                                    }
                                                    self.googleMap.animate(with: GMSCameraUpdate.fit(bounds))
                                                    //  self.googleMap.animate(toZoom: 14)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            } catch let myJSONError {
                print(myJSONError)
            }
            }.resume()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
         dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
         dismiss(animated: true, completion: nil)
    }
   
    func getValuesForOriginAndDesString(){
        print(origin?.coordinate.latitude)
        print(destination?.coordinate.latitude)
        
        if self.rideTypeLabel.text! == "Ride Type" || self.noOfRidersLabel.text! == "No of Riders" || endLocation.text == "Choose drop-off location" || startLocation.text == "Choose pick-up location" {

            return
        }
      
        if self.orText != nil && self.desText != nil {
            self.toGetPackages()

        }
        else{
            getCityString(start:(origin?.coordinate.latitude)!,end:(origin?.coordinate.longitude)!,type:0)
            getCityString(start:(destination?.coordinate.latitude)!,end:(destination?.coordinate.longitude)!,type:1)

        }
//      getPackages()
        
    }
    func getPackages(){
        
         self.scrollView.setContentOffset(CGPoint(x:0,y: self.scrollView.contentSize.height - UIScreen.main.bounds.height + 40), animated: true)
        self.scrollView.isScrollEnabled = true
        self.tableViewForCommuterPkgs.reloadData()
    }
    
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CommuterPackagesViewCell
        
        cell.packageName.text! = self.packages[indexPath.row].name
        cell.priceLbl.text! = "\(self.packages[indexPath.row].amount)"

        cell.totalNoOfTrips.text! = "\(self.packages[indexPath.row].numberOfRides)"
        cell.totalNoOfDays.text! = "\(self.packages[indexPath.row].packageValidity)"
        
//        cell.textLabel?.text = "sdfghjk"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "BookRideViewController") as! BookRideViewController
        
        vc.amountText  = "$"  + "\(self.packages[indexPath.row].amount)"
        vc.typeText = self.packages[indexPath.row].packageType
        vc.choosePickUpText = origin?.formattedAddress
        vc.sourceString = origin?.formattedAddress
        vc.sourceLng = String(((origin?.coordinate.longitude)! as Double))
        vc.sourceLat = String(((origin?.coordinate.latitude)! as Double))
        vc.chooseDropText = destination?.formattedAddress
        vc.detinationString = destination?.formattedAddress
        vc.destinationLng = String(((destination?.coordinate.longitude)! as Double))
        vc.destinationLat = String(((destination?.coordinate.latitude)! as Double))
        
        vc.totalDaysText = "\(self.packages[indexPath.row].packageValidity)"
        vc.totalTripText = "\(self.packages[indexPath.row].numberOfRides)"
        vc.packageID = "\(self.packages[indexPath.row].packageId)"
        vc.routeID = "\(self.packages[indexPath.row].routeId)"
        
        if rideTypeLabel.text == "RIDE-SHARE" {
            
            vc.rideType = "RIDE_SHARE"
        }else{
            vc.rideType = "CHARTER"
        }
        
        vc.numberOfRiders = String(describing: noOfRidersLabel.text?.characters.first! as AnyObject)
//        self.navigationController?.pushViewController(vc, animated: true)

        self.present(vc, animated: true, completion: nil)
        
    }
}

