//
//  LoginViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material
import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
import TwitterCore
import Alamofire

class LoginViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate{

    @IBOutlet var topConstraintTextField: NSLayoutConstraint!
    var window: UIWindow?
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: ErrorTextField!
   // let loginManager = FBSDKLoginManager()
    @IBOutlet weak var sendBtn: Button!
    @IBOutlet weak var emailTextField: ErrorTextField!
     var show: [String: AnyObject]!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let width = UIScreen.main.bounds.width
        print(width)
        if width >= 350 && width <= 400 {
            
            topConstraintTextField.constant = 70
        }
        
        CommonFunctions.sharedInstance.prepareButton(vc: self,btn:sendBtn)
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:emailTextField ,placeHolderText:"Username",detailFieldName:"Enter Username")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:passWordTextField,placeHolderText:"Password",detailFieldName:"nter Password")
         CommonFunctions.loadTokenOnAppLoad()
        
        if CommonFunctions.userToken != nil{
            
            let leftViewController = {
                return UIStoryboard.viewController(identifier: "SideMenuViewController") as! SideMenuViewController
            }()
            
            window = UIWindow(frame: Screen.bounds)
            
            let loginController: HomeViewController = {
                return UIStoryboard.viewController(identifier: "HomeViewController") as! HomeViewController
            }()
            let appToolbarController1 = AppToolbarController(rootViewController: loginController)
            
            self.window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
            
            let appNavigationDrawerController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
            // appNavigationDrawerController.isLeftViewEnabled = false
            self.window!.rootViewController = appNavigationDrawerController
            //        window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1)
            
            window!.makeKeyAndVisible()
            UIApplication.shared.statusBarStyle = .lightContent
        }
        
        
//        if CommonFunctions.sharedInstance.getValueFromUserDefaultsForKey(IS_LOGIN) != nil {
//            if CommonFunctions.sharedInstance.getValueFromUserDefaultsForKey(IS_LOGIN) as! Bool == true {
//                //            let vc =  self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                //
//                //            self.present(vc, animated: true)
//
//                self.performSegue(withIdentifier: "toHomeByLogin", sender: nil)
//
//            }
//        }
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
       // GIDSignIn.sharedInstance().clientID = " 257290388932-copuiq2akrtur1ijfs6g08l2mkqrhu1j.apps.googleusercontent.com"
       // "984600575918-cshgkmhn6qo62gj92neam97j7doqies7.apps.googleusercontent.com"
        //GIDSignIn.sharedInstance().serverClientID = "1077717045261-03uepvr2rv3kae8mrfeqav10t13vkgut.apps.googleusercontent.com"
//        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
//        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionOnGoogleBtn(_ sender: Any) {
        
        print("google login is in progress........")
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().signOut()
        
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        //Loader.stopLoading()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("user info:- \(user)")
        if (error == nil) {
            let userId = user.userID
            let idToken = user.authentication.idToken
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            let serverid = 123456
            print("server id:-  \(serverid)")
            
            if user.profile.hasImage {
                let image = user.profile.imageURL(withDimension: 150)
                print("image from user:- \(image?.absoluteString ?? "googleimage")")
                UserDefaults.standard.setValue(image?.absoluteString, forKey: "imageFromGoogle")
            }
            print("Welcome: ,\(userId ?? "userId"), \(idToken ?? "idToken"), \(fullName ?? "asdfghj"), \(givenName ?? "givenName"), \(familyName ?? "familyName"), \(email ?? "email")")
            print("userid:-\(userId!)")
            print("token id:-\(idToken!)")
            let password = "\(serverid);" + idToken!;
            print("password is:- \(password)")
            UserDefaults.standard.setValue(password, forKey: "password")
            UserDefaults.standard.setValue(userId!, forKey: "googleid")
            UserDefaults.standard.setValue(idToken!, forKey: "googletoken")
            UserDefaults.standard.setValue(fullName!, forKey: "fullName")
            UserDefaults.standard.setValue(email!, forKey: "googleEmail")
            UserDefaults.standard.setValue(email!, forKey: "givenName")
            UserDefaults.standard.synchronize()
            self.checkForLoginFromGoogle(email: UserDefaults.standard.string(forKey: "googleid")!, password: UserDefaults.standard.string(forKey: "password")!)
        }
        
    }
    
    func checkForLoginFromGoogle(email: String, password: String){
        let login = LoginVM()
        login.username = email
        login.password = password
        login.loginType = "GOOGLE"
        
        APIManger.login(view: self, login: login) {
            (completion: TokenDTO) in
            print(completion)
            
            let homeVC: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            let appToolbarController1 = AppToolbarController(rootViewController: homeVC)
            appToolbarController1.prepare()
            newWindow!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1)
            
            newWindow!.makeKeyAndVisible()
            UIApplication.shared.statusBarStyle = .lightContent
            
        }
        
    }
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        //        Loader.startLoading()
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        //Loader.stopLoading()
    }
    @IBAction func didTapOnLoginBtn(_ sender: Any) {
        
        if emailTextField.text!.trimmingCharacters(in: .whitespaces) == "" {
            emailTextField.isErrorRevealed = true
            return
        }
        if passWordTextField.text!.trimmingCharacters(in: .whitespaces) == "" {
            passWordTextField.isErrorRevealed = true
            return
        }
            
        else{
        authenticateUser()
        }
     
    }
    
    func authenticateUser() {
        
        let login = LoginVM()
        login.username = emailTextField.text!
        login.password = passWordTextField.text!
        
        APIManger.login(view: self, login: login){
            (completion: TokenDTO) in
            print(completion)
            self.moveToHomeScreen()
            
        }
    }
    
    @IBAction func actionOnFacebookBtn(_ sender: Any) {
        print("facebook login is in process.....")
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email", "public_profile", "user_location"], from: self) { (result, error) in
            if (error == nil) {
                let fbloginresult: FBSDKLoginManagerLoginResult = result!
                print("there is result....\(result!)")
                if fbloginresult.grantedPermissions != nil {
                    
                    //  if(fbloginresult.grantedPermissions.contains("email"))
                    
                    if (fbloginresult.grantedPermissions.contains("email")) {
                        print("app in fbsdk login manager.....\(fbloginresult.grantedPermissions.contains("email"))")
                        print(fbloginresult.token.tokenString)
                        print(fbloginresult.token.userID)
                        UserDefaults.standard.setValue(fbloginresult.token.tokenString, forKey: "token")
                        UserDefaults.standard.synchronize()
                        self.getUserData()
                        loginManager.logOut()
                    }
                }
            }
        }
    }
    func getUserData() {
        
        if ((FBSDKAccessToken.current()) != nil) {
            print("token.......\(FBSDKAccessToken)")
            let parameters = ["fields": "id, name, first_name, last_name, picture.type(large), email, cover"]
            FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: {
                (connection, result, error) -> Void in
                if (error == nil) {
                    self.show = result as! [String: AnyObject]
                    print(result!)
                    print(self.show)
                    if let responseDictionary = result as? NSDictionary {
                        print(responseDictionary);
                        let id = responseDictionary["id"] as? String
                        print(id ?? "no id")
                        let email = responseDictionary["email"] as? String
                        print(email ?? "no email")
                        let first = responseDictionary["name"] as? String
                        print(first ?? "no name")
                        let firstName = responseDictionary["first_name"] as? String
                        print(firstName ?? "no first_name")
                        let lastName = responseDictionary["last_name"] as? String
                        print(lastName ?? "no last_name")
                        
                        if let picture = responseDictionary["cover"] as? NSDictionary {
                            if let coverPicture = picture["source"] as? String {
                                print(coverPicture)
                                UserDefaults.standard.setValue(coverPicture, forKey: "cover")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        if let picture = responseDictionary["picture"] as? NSDictionary {
                            if let data = picture["data"] as? NSDictionary {
                                if let profilePicture = data["url"] as? String {
                                    print(profilePicture)
                                    UserDefaults.standard.setValue(profilePicture, forKey: "image")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                        UserDefaults.standard.setValue(firstName, forKey: "firstName")
                        UserDefaults.standard.setValue(lastName, forKey: "lastName")
                        UserDefaults.standard.setValue(email, forKey: "fbEmail")
                        UserDefaults.standard.setValue(id, forKey: "id")
                        UserDefaults.standard.synchronize()
                        
                        print("id is...\(UserDefaults.standard.string(forKey: "id")!)")
                        self.checkForLogin(email: UserDefaults.standard.string(forKey: "id")!, password: UserDefaults.standard.string(forKey: "token")!)
                    }
                }
            })
        }
    }
    
    func checkForLogin(email: String, password: String) {
        
        let login = LoginVM()
        login.username = email
        login.password = password
        login.loginType = "FACEBOOK"
        
        UserDefaults.standard.setValue(email, forKey: "username")
        UserDefaults.standard.synchronize()

        APIManger.login(view: self, login: login) {
            (completion: TokenDTO) in
            print(completion)
    
//            APIManger.getProfile(view: self){
//                (completion: GetProfileDTO) in
//                print(completion)
            
                        let homeVC: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        //homeVC.isFromUser = true
                        let appToolbarController1 = AppToolbarController(rootViewController: homeVC)
                        appToolbarController1.prepare()
                        //sideBarViewController.isFromStylist = true
                        //self.navigationDrawerController?.isLeftViewEnabled = true
                        newWindow!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1)
                        
                        newWindow!.makeKeyAndVisible()
                        UIApplication.shared.statusBarStyle = .lightContent

         }
    }
    
    @IBAction func actionOnTwitterBtn(_ sender: Any) {
       // TWTRTwitter.sharedInstance().start(withConsumerKey:consumerSecret:)
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                print("signed in as \(session?.userName)");
                let client = TWTRAPIClient.withCurrentUser()
                
                client.requestEmail { email, error in
                    if (email != nil) {
                        print("signed in as \(session?.userName)");
                    } else {
                        print("error: \(error?.localizedDescription)");
                    }
                }
                
            } else {
                print("error: \(error?.localizedDescription)");
            }
        })
       
        
    }
    
    func moveToHomeScreen(){
        
        let leftViewController = {
            return UIStoryboard.viewController(identifier: "SideMenuViewController") as! SideMenuViewController
        }()
        
        window = UIWindow(frame: Screen.bounds)
        
        let loginController: HomeViewController = {
            return UIStoryboard.viewController(identifier: "HomeViewController") as! HomeViewController
        }()
        let appToolbarController1 = AppToolbarController(rootViewController: loginController)
        
        self.window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
        
        let appNavigationDrawerController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
    
        self.window!.rootViewController = appNavigationDrawerController
        
        window!.makeKeyAndVisible()
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}

