//
//  MyPackagesViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//
import Foundation
import UIKit
import Material

class MyPackagesViewController: UIViewController {

  
    @IBOutlet var tableViewForPackages: UITableView!
    var getData = MyPackagesDTO()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewForPackages.delegate = self
        tableViewForPackages.dataSource = self
        getURL()

    }
    
    func getURL(){
        
     APIManger.myPackages(view: self, clientPackageStatus: "CURRENT", page: "0", size: "20"){
            
        (completion: MyPackagesDTO) in
            print(completion)
            self.getData = completion
        
                DispatchQueue.main.async {
                    self.tableViewForPackages.reloadData()
                }
        
         }
     
    }
    
    private func preparePageTabBarItem() {
        

        pageTabBarItem.title = "Current"
        pageTabBarItem.titleColor = .white
        pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
    
   }
    
}
extension MyPackagesViewController: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getData.content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyPackagesViewCell
        
//        cell.viewForList.layer.cornerRadius = 4
//        cell.viewForList.clipsToBounds = true
//        cell.viewForList.layer.shadowOffset.width = 2
//        cell.viewForList.layer.shadowOffset.height = 1
//
//        cell.viewForList.layer.shadowColor = UIColor.lightGray.cgColor
//
//        cell.viewForList.layer.shadowOpacity = 0.7
//        cell.viewForList.layer.shadowRadius = 5
        cell.packageName.text! = self.getData.content[indexPath.row].name
        cell.noOfTripsLeft.text! = "\(self.getData.content[indexPath.row].totalRides)"
        cell.priceLbl.text = "$ " + "\(self.getData.content[indexPath.row].packagePrice)"
        let date = Date(timeIntervalSince1970: self.getData.content[indexPath.row].expiryDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "dd-MM-yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        cell.expiryDate.text = strDate
        cell.shareBtn.addTarget(self, action: #selector(handleSelectionOfShare(sender:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func handleSelectionOfShare(sender: UIButton){
        
       let index = self.tableViewForPackages.indexPath(for: sender.superview?.superview?.superview as! MyPackagesViewCell)?.row
        let vc = storyboard?.instantiateViewController(withIdentifier: "SearchMemberViewController") as! SearchMemberViewController
        vc.clientPkgId = "\(self.getData.content[index!].clientPackageId)"
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            return 105
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("ohhhh nōoooo.... you cant move forward from here ")
    }
    
}
