//
//  MyRideViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class MyRideViewController: UIViewController {

    @IBOutlet var tableViewForRides: UITableView!
    
    @IBOutlet var headingLbl: UILabel!
    var data = ActiveRidesDTO()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewForRides.delegate = self
        tableViewForRides.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }

    func getData(){
        
        APIManger.ridesPackages(view: self, page: "0", size: "20"){
             (completion: ActiveRidesDTO) in
             print(completion)
             self.data = completion
             DispatchQueue.main.async {
                self.tableViewForRides.reloadData()
            }
        }
    }
    
    
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}

extension MyRideViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyRidesCell
        let date = Date(timeIntervalSince1970: self.data.content[indexPath.row].bookingTime)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        cell.dateLbl.text! = strDate
        cell.destinationLbl.text = self.data.content[indexPath.row].destination
        cell.sourceLbl.text = self.data.content[indexPath.row].source
        if self.data.content[indexPath.row].tip == nil{
            cell.DriverTipLbl.text = "Driver Tip:-  " + "\(0)"
        }
        else{
        cell.DriverTipLbl.text = "Driver Tip:-  " + "$ " + "\(self.data.content[indexPath.row].tip!)"
        }
        cell.packageNameLbl.text! = self.data.content[indexPath.row].packageName
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SingleDetailViewController") as! SingleDetailViewController
        vc.id = "\(self.data.content[indexPath.row].tripId)"
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
}
