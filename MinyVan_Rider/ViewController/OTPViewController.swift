//
//  OTPViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class OTPViewController: UIViewController {

    @IBOutlet weak var enterOtpTexField: ErrorTextField!
    
    @IBOutlet weak var passwordTextField: ErrorTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:enterOtpTexField ,placeHolderText:"Enter Otp",detailFieldName:"Error, incorrect Otp")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:passwordTextField,placeHolderText:"Password",detailFieldName:"Error, Password")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
            
         enterOtpTexField.keyboardType = .numberPad
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapNextBtnAction(_ sender: Any) {
        if enterOtpTexField.text == "" {
            
            enterOtpTexField.isErrorRevealed = true
            return
        }
        if passwordTextField.text == "" {
            
            passwordTextField.isErrorRevealed = true
            return
        }
        
        sendOTP()
    }
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func sendOTP(){
        let reset = ResetPasswordVM()
        reset.key = enterOtpTexField.text!
        reset.newPassword = passwordTextField.text!
        
        APIManger.resetPassword(view: self, resetPW: reset){
            (completion: ForgotPassDTO) in
            self.createAlert(title: "", body: completion.reason)
            
            
            let loginController: LoginViewController = {
                return UIStoryboard.viewController(identifier: "LoginViewController") as! LoginViewController
            }()
            
            newWindow!.rootViewController = loginController
            
            newWindow!.makeKeyAndVisible()
            UIApplication.shared.statusBarStyle = .lightContent
            
        }
        
    }

}
