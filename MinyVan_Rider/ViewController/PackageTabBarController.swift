//
//  PackageTabBarController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class PackageTabBarController: PageTabBarController {
    
    static let appClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    
    open override func prepare() {
        super.prepare()
        preparePageTabBar()
        //pageTabBar
    }
    
    private func preparePageTabBar() {
        //LoginTabBarViewController.darkblue
        pageTabBar.backgroundColor = PackageTabBarController.appClr//
        // pageTabBar.tintColor = LoginTabBarViewController.blue
        self.pageTabBarAlignment = PageTabBarAlignment.top
        pageTabBar.lineAlignment = .bottom
        // pageTabBar.dividerColor = .white
        pageTabBar.lineColor = .white
        pageTabBar.tintColor = PackageTabBarController.appClr
        
    }
    
}

extension PackageTabBarController: PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
        
        print("pageTabBarController", pageTabBarController, "didTransitionTo viewController:", viewController)
    }
    
}


