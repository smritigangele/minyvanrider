//
//  PackagesTabsViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class PackagesTabsViewController: UIViewController {

    static let appClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    
    @IBOutlet var ContainerForViews: UIView!
    var isFromShared = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromShared == true{
            let byMe = {
                return UIStoryboard.viewController(identifier: "SharedPackagesViewController") as! SharedPackagesViewController
            }()
            
            let withMe = {
                
                return UIStoryboard.viewController(identifier: "SharedWithMeViewController") as! SharedWithMeViewController
            }()
            let sharedPkgs:PackageTabBarController = PackageTabBarController(viewControllers: [byMe, withMe], selectedIndex: 0)
            
            add(asChildViewController: sharedPkgs)
            
        }
            
        else{
            
        let currentPkg = {
            return UIStoryboard.viewController(identifier: "MyPackagesViewController") as! MyPackagesViewController
        }()
        
        let completedPkg = {
            
            return UIStoryboard.viewController(identifier: "CompletedPackagesViewController") as! CompletedPackagesViewController
        }()
        
        let expiredPkg = {
            
            return UIStoryboard.viewController(identifier: "ExpiryPackagesViewController") as! ExpiryPackagesViewController
        }()
        
        let packages:PackageTabBarController = PackageTabBarController(viewControllers: [currentPkg, completedPkg, expiredPkg], selectedIndex: 0)
         
        add(asChildViewController: packages)
            
        }
        
    }

   
    @IBAction func didtapOnBackBtn(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        
       ContainerForViews.addSubview(viewController.view)
        viewController.view.frame = ContainerForViews.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
        
    }
    
}


