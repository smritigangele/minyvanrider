//
//  PrivacT&CAndFAQViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit

class PrivacT_CAndFAQViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    var url: String!
    
    @IBOutlet var headerTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://developer.apple.com/")
        webView.loadRequest(URLRequest(url: url!))
        webView.delegate  = self
        
    }
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        
    self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
