//
//  SearchMemberViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//
import Foundation
import UIKit
import Material

class SearchMemberViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
        var clientPkgId: String!
        var getListOfMembers = GetSearchMemeberList()
        let selectedClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
        @IBOutlet weak var tblSearchResults: UITableView!
    
        @IBOutlet var noOfRidesToShare: UIView!
    
        @IBOutlet var noOfRidesTextField: TextField!
    
      var blurEffectViewS = UIVisualEffectView()
        var dataArray = [String]()
        
        var filteredArray = [GetProfileDTO]()
        
        var shouldShowSearchResults = false
        
    @IBOutlet var searchBar: UISearchBar!
    //var searchController: UISearchController!
        
//        var customSearchController: CustomSearchController!
    
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            
            tblSearchResults.delegate = self
            tblSearchResults.dataSource = self
            searchBar.delegate = self
           // filteredArray = getListOfMembers.content
            
           // configureCustomSearchController()
        }
    
        // MARK: UITableView Delegate and Datasource functions
        
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if shouldShowSearchResults {
                return filteredArray.count
            }
            else {
                return self.getListOfMembers.content.count
            }
        }
        
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
            
            if shouldShowSearchResults {
                cell.textLabel?.text = self.getListOfMembers.content[indexPath.row].name
            }
            else {
                cell.textLabel?.text = self.getListOfMembers.content[indexPath.row].name
            }
            
            return cell
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        openNoOfRidesToShare()
        
    }
    
    func openNoOfRidesToShare(){
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = UIScreen.main.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        blurEffectView.alpha = 0.5
        self.blurEffectViewS = blurEffectView
        self.view.addSubview(blurEffectView)
        self.view.bringSubview(toFront:blurEffectView )
        noOfRidesToShare.frame = CGRect(x: (UIScreen.main.bounds.width - noOfRidesToShare.frame.width)/2, y: 200, width: noOfRidesToShare.frame.width, height: noOfRidesToShare.frame.height)
        noOfRidesToShare.layer.cornerRadius = 5
        noOfRidesToShare.clipsToBounds = true
        noOfRidesTextField.isUserInteractionEnabled = true
        
        self.view.addSubview(self.noOfRidesToShare)
        self.view.bringSubview(toFront:self.noOfRidesToShare )
        
    }
    
    @IBAction func actionOnDoneBtn(_ sender: Any) {
        
        let sharePkgs = SharePackageVM()
        sharePkgs.clientPackageId = Int(self.clientPkgId)!
        sharePkgs.ridesToShare = Int(self.noOfRidesTextField.text!)!
        sharePkgs.username = searchBar.text!
        
        if self.noOfRidesTextField.text == ""{
           self.createAlert(title: "", body: "Please Enter the no of rides you want to share")
            return
        }
            
        else{
            
        APIManger.sharePackages(view: self, share: sharePkgs)
        {
            (completion: ForgotPassDTO) in
            print(completion)
            self.createAlert(title: "", body: completion.reason)
        }
        
        blurEffectViewS.removeFromSuperview()
        noOfRidesToShare.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
        noOfRidesTextField.text = ""
        }
        
    }

    @IBAction func actionOnCancelBtn(_ sender: Any) {
        
        blurEffectViewS.removeFromSuperview()
        noOfRidesToShare.removeFromSuperview()
        
        noOfRidesTextField.text = ""
        
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
            return 60.0
        }

        // MARK: Custom functions
        
        func loadListOfMember() {
            
            APIManger.getSearchMember(view: self, searchText: searchBar.text!){
                (completion: GetSearchMemeberList) in
                print(completion)
                self.getListOfMembers = completion
                DispatchQueue.main.async {
                     self.tblSearchResults.reloadData()
                }
          }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        loadListOfMember()
        filteredArray = searchText.isEmpty ? getListOfMembers.content : self.getListOfMembers.content.filter({ (name) -> Bool in
                            let memberText = name
     
            return ((memberText.name.range(of: searchText, options: .caseInsensitive) != nil))
                        })
            // If dataItem matches the searchText, return true to include it
//            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        
    
        
        tblSearchResults.reloadData()
    }

    
    @IBAction func actionOnBackBtn(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}

