//
//  SettingsViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class SettingsViewController: UIViewController {

    @IBOutlet var backBtn: Button!
    
    @IBOutlet var changePW: UILabel!
    @IBOutlet var editProfile: UILabel!
    
    @IBOutlet var logoutLbl: UILabel!
  //  @IBOutlet var logoutBtn: Button!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logout = UITapGestureRecognizer(target: self, action: #selector(logoutBtn))
        logoutLbl.isUserInteractionEnabled = true
        logoutLbl.addGestureRecognizer(logout)
        
        let editProfileLbl = UITapGestureRecognizer(target: self, action: #selector(actionOnEditProfile))
        editProfile.isUserInteractionEnabled = true
        editProfile.addGestureRecognizer(editProfileLbl)
        
        let changePasswordLbl = UITapGestureRecognizer(target: self, action: #selector(change_Password))
        changePW.isUserInteractionEnabled = true
        changePW.addGestureRecognizer(changePasswordLbl)
   // self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func change_Password(sender:UITapGestureRecognizer){
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func actionOnEditProfile(sender:UITapGestureRecognizer){
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        //vc.isFromEditProfile = true
        self.present(vc, animated: true, completion: nil)
    }
    
    func logoutBtn(sender:UITapGestureRecognizer){
        
        CommonFunctions.logout(view: self)
        
        let loginController: LoginViewController = {
            return UIStoryboard.viewController(identifier: "LoginViewController") as! LoginViewController
        }()
        
        newWindow!.rootViewController = loginController
        newWindow!.makeKeyAndVisible()
        //self.dismiss(animated: true, completion: nil)
    
    }
    
    @IBAction func didTapOnBackBtn(_ sender: Any) {
        
       self.dismiss(animated: true, completion: nil)
        
    }

}
