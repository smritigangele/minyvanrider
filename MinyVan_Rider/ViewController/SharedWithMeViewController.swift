//
//  SharedWithMeViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit

class SharedWithMeViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
     var resultOfSharedData = SharedPackagesDTO()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    private func preparePageTabBarItem() {
        
        //        if pageIndex?.selectedIndex == 0{
        
        pageTabBarItem.title = "With Me"
        pageTabBarItem.titleColor = .white
        pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      tableView.delegate = self
      tableView.dataSource = self
      tableView.separatorStyle = .none
      get_User_Data()
        
    }
    
    func get_User_Data(){
        
        APIManger.sharedPackgeWithMe(view: self){
            (completion: SharedPackagesDTO) in
            print(completion)
            self.resultOfSharedData = completion
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
        
    }
}

extension SharedWithMeViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultOfSharedData.content.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SharedWithMeCell
        cell.packageName.text = self.resultOfSharedData.content[indexPath.row].name
        cell.priceLbl.text = "$ " +  "\(self.resultOfSharedData.content[indexPath.row].packagePrice)"
        cell.noOfRideShared.text = "\(self.resultOfSharedData.content[indexPath.row].totalRides)"
        let date = Date(timeIntervalSince1970: self.resultOfSharedData.content[indexPath.row].expiryDate)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateStyle = .medium
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        cell.expiryLbl.text = strDate
        cell.sharedWithNameLbl.text = self.resultOfSharedData.content[indexPath.row].sharedBy
        cell.mobileNumberLbl.text = self.resultOfSharedData.content[indexPath.row].sharedByMobile
        cell.shareduserEmail.text = self.resultOfSharedData.content[indexPath.row].sharedByEmail
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 184
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("sdfgyuifghjk")
        
   }
}
