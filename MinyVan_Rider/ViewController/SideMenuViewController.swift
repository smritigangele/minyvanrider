//
//  SideMenuViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 08/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    let list = ["Active Rides","My Packages","My Rides","Shared Packages","Settings","Share"]
    
    let images = ["my_rides","my_packages","my_rides","my_packages","setting","share"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       tableView.delegate = self
     tableView.dataSource = self
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension SideMenuViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuViewCell
        
        cell.nameLbl.text = list[indexPath.row]
        let image = images[indexPath.row]
        cell.imageOfNameLbl.image = UIImage(named: image)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("asdfghjk")
        actionOnCell(indexPath.row)
        
}
    func actionOnCell(_ index: Int){
        
        switch index {
        
        case 0:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "ActiveRidesViewController") as! ActiveRidesViewController
             self.present(vc, animated: true, completion: nil)
            
        case 2:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "MyRideViewController") as! MyRideViewController
            self.present(vc, animated: true, completion: nil)
         
        case 1:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "PackagesTabsViewController") as! PackagesTabsViewController
          self.present(vc, animated: true, completion: nil)
        
        case 3:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "PackagesTabsViewController") as! PackagesTabsViewController
            vc.isFromShared = true
            self.present(vc, animated: true, completion: nil)
        
        case 4:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let vc = storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.present(vc, animated: true, completion: nil)
            
        case 5:
        self.navigationDrawerController?.navigationDrawerController?.closeLeftView()
            let textToShare = "This would be MinyVan app url"
            let text = [textToShare]
            let activityViewController = UIActivityViewController(activityItems: text, applicationActivities: nil)
            present(activityViewController, animated: true, completion: {})
       
        default:
            
            break;
        
        }
    }
}
