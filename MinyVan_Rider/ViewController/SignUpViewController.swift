//
//  SignUpViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 07/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import Material

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var nameTextFiled: ErrorTextField!
    @IBOutlet var bckGroundImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var passwordField: ErrorTextField!
    @IBOutlet weak var userNameTextField: ErrorTextField!
    @IBOutlet weak var emailTextField: ErrorTextField!
    @IBOutlet weak var mobileTextFiled: ErrorTextField!
    var window: UIWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:emailTextField ,placeHolderText:"Email",detailFieldName:"Enter Email")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:passwordField,placeHolderText:"Password",detailFieldName:"Enter Password")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:mobileTextFiled ,placeHolderText:"Mobile",detailFieldName:"Enter Mobile Number")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:userNameTextField,placeHolderText:"Username",detailFieldName:"Enter Username")
        CommonFunctions.sharedInstance.prepareTextlField(vc:self,textField:nameTextFiled,placeHolderText:"Name",detailFieldName:"Enter Name")
           let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
           mobileTextFiled.keyboardType = .numberPad
           emailTextField.delegate = self
           passwordField.delegate = self
           mobileTextFiled.delegate = self
           userNameTextField.delegate = self
           nameTextFiled.delegate = self
           self.view.insertSubview(bckGroundImage, at: 0)
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        scrollView.contentSize.width = self.view.frame.width
//          scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 800)
          view.addGestureRecognizer(tap)
        
    }
    
    @IBAction func didTapBackBtn(_ sender: Any) {
       // self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @objc override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        DispatchQueue.main.async {
            let scrPt = CGPoint(x: 0, y: textField.frame.origin.y/4)
           // self.scrollView!.setContentOffset(scrPt, animated: true)
        }
    }
    
    override func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 0, animations:{ self.scrollView!.setContentOffset(CGPoint.zero, animated: true)})
//        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func didTapSignUpBtn(_ sender: Any) {
        
        if nameTextFiled.text!.trimmingCharacters(in: .whitespaces) == "" {
            nameTextFiled.detail = "Enter Name"
            nameTextFiled.isErrorRevealed = true
            return
        }
        if nameTextFiled.text!.trimmingCharacters(in: .whitespaces).characters.count < 5 {
            nameTextFiled.detail = "Name size must Between 5 and 50"
            nameTextFiled.isErrorRevealed = true
            return
        }
        
        if mobileTextFiled.text!.trimmingCharacters(in: .whitespaces) == "" {
            mobileTextFiled.isErrorRevealed = true
            return
        }
        
        if emailTextField.text!.trimmingCharacters(in: .whitespaces) == "" {
            emailTextField.isErrorRevealed = true
            return
        }
        
        if userNameTextField.text!.trimmingCharacters(in: .whitespaces) == "" {
            userNameTextField.isErrorRevealed = true
            return
        }
        
        if passwordField.text!.trimmingCharacters(in: .whitespaces) == "" {
            passwordField.isErrorRevealed = true
            return
        }
        registerUser()
    }
    
    func registerUser(){
        
        let signup = SignupVM()
        signup.name = nameTextFiled.text!
        signup.username = userNameTextField.text!
        signup.email = emailTextField.text!
        signup.mobile = mobileTextFiled.text!
        signup.password = passwordField.text!
       
        APIManger.signup(view: self, signup: signup, accountType: "NORMAL"){
            (completion: TokenDTO) in
        print(completion)
        self.registerDevice()
        self.moveToHomeScreen()
       }
    }
    
    func registerDevice(){
        
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        print(uuid ?? "device type::::::")
        let  deviceName = UIDevice.current.name
        let deviceRegister: AddDeviceVM = AddDeviceVM()
        deviceRegister.deviceType = "IOS"
        deviceRegister.deviceName = deviceName
        deviceRegister.messagingID = deviceTokenApp
        print("device token \(deviceTokenApp)")
        deviceRegister.deviceID = uuid!
        
        APIManger.addDevice(view: self, device: deviceRegister){
            (completion: ForgotPassDTO) in
            print(completion)
        }
        
    }
    
    func moveToHomeScreen(){
            
            let leftViewController = {
                return UIStoryboard.viewController(identifier: "SideMenuViewController") as! SideMenuViewController
            }()
            
            window = UIWindow(frame: Screen.bounds)
            
            let loginController: HomeViewController = {
                return UIStoryboard.viewController(identifier: "HomeViewController") as! HomeViewController
            }()
            let appToolbarController1 = AppToolbarController(rootViewController: loginController)
            
            self.window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
            
            let appNavigationDrawerController = AppNavigationDrawerController(rootViewController: appToolbarController1, leftViewController: leftViewController)
            // appNavigationDrawerController.isLeftViewEnabled = false
            self.window!.rootViewController = appNavigationDrawerController
            //        window!.rootViewController = AppNavigationDrawerController(rootViewController: appToolbarController1)
            
            window!.makeKeyAndVisible()
            UIApplication.shared.statusBarStyle = .lightContent
        
        }
    
}

