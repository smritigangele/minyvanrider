//
//  SingleDetailViewController.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class SingleDetailViewController: UIViewController,GMSMapViewDelegate{

    let selectedClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    
    @IBOutlet var dropOffTimeLbl: UILabel!
    @IBOutlet var pickupTimeLbl: UILabel!
    @IBOutlet var sourcePointOfTrip: UILabel!
    @IBOutlet var destinationPointOfTrip: UILabel!
    
    @IBOutlet var timePeriod: UILabel!
    
    @IBOutlet var googleMap: GMSMapView!
    var userData = RiderPackageDTO()
    var id: String!
    var isPickUpSelceted = true
    var destination:GMSPlace?
    var origin:GMSPlace?
    var desText:String?
    var orText:String?
    var fromNotification = false
    var tripId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        let myString:NSString = "3 mins away"
        var myMutableString = NSMutableAttributedString()
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 16.0)!])
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: selectedClr, range: NSRange(location:0,length:1))
        timePeriod.attributedText = myMutableString
        googleMap.delegate = self
        googleMap.isMyLocationEnabled = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        get_Data()
    }
    
    func get_Data(){
        
        if fromNotification == true{
            
            
            
        }
        else{
            
        APIManger.singleRideDetail(view: self, id: self.id){(completion: RiderPackageDTO) in
            print(completion)
            self.userData = completion
            self.getDateAndTime(ts: self.userData.plannedDropOffTime)
            self.toGetTime(ts: self.userData.plannedDropOffTime)
           
            self.sourcePointOfTrip.text! = self.userData.source
            self.destinationPointOfTrip.text! = self.userData.destination
            self.drawableRoute()
        }
      }
    }
    func getDateAndTime(ts: Double){
        
        let date = Date(timeIntervalSince1970: ts)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateStyle = .medium
        dayTimePeriodFormatter.timeStyle = .medium
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY 'at' hh:mm"
        
        let dateString = dayTimePeriodFormatter.string(from: date)
       // self.dropOffTimeLbl.text! =
        self.pickupTimeLbl.text! = dateString
        print( " _ts value is \(dateString)")
        
        
    }
    
    func toGetTime(ts: Double){
        
        let date = Date(timeIntervalSince1970: ts)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.timeStyle = .medium
        dayTimePeriodFormatter.dateFormat = "hh:mm:aa"
        let dateString = dayTimePeriodFormatter.string(from: date)
        let time = dateString.split(separator: ":")
        let hour = time[0]
        let min = time[1]
        let sec = time[2]
        print("current time \(hour) \(min) \(sec)")
        
        let hh2 = (Calendar.current.component(.hour, from: Date()))
        let mm2 = (Calendar.current.component(.minute, from: Date()))
        let ss2 = (Calendar.current.component(.second, from: Date()))
       
//        print(hh2 + mm2 + ss2)
//        let strng = "\(hh2:mm2:ss2)"
//        let dateAsString = strng
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "HH:mm"
//        
//        let date1 = dateFormatter.date(from: dateAsString)
//        dateFormatter.dateFormat = "h:mm a"
//        let Date12 = dateFormatter.string(from: date1!)
//        print("12 hour formatted Date:",Date12)
        
        if hh2 > Int(hour)!{
            let finalResult = (((hh2 - Int(hour)!)+((Int(min)! - mm2))/60))
            print("final time - \(finalResult)")
        }
        else{
            let finalResult = (((Int(hour)! - hh2)+((Int(min)! - mm2))/60))
            print("final time - \(finalResult)")
        
        }
    }
    func drawableRoute(){
        
        let camera = GMSCameraPosition.camera(withLatitude:self.userData.pickUpLat, longitude: self.userData.pickUpLong, zoom: 13)
        //self.googleMap.myLocationEnabled = true
        
        self.googleMap.camera = camera
        
        if isPickUpSelceted {
            
            //startLocation.text = place.formattedAddress
//            origin =
//            print("origin:- \(origin)")
            
            let marker = GMSMarker()
            marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "startLocation"), scaledToSize: CGSize(width: 60, height: 60))
            marker.position = CLLocationCoordinate2DMake(self.userData.pickUpLat,self.userData.pickUpLong)
            marker.title = self.userData.source
            marker.map = self.googleMap
            if self.userData.source != nil && self.userData.destination != nil {
                getRoute()
            }
        }else{
           
            let marker = GMSMarker()
            marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "endLocation"), scaledToSize: CGSize(width: 60, height: 60))
            marker.position = CLLocationCoordinate2DMake(self.userData.dropOffLat,self.userData.dropOffLong)
            marker.title = self.userData.destination
            marker.map = self.googleMap
            //destination = place
            if self.userData.source != "" && self.userData.destination != "" {
                getRoute()
            }
        }
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        print("image of marker:- \(image)  &  \(newSize)")
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func getRoute(){
        
        googleMap.clear()
        
        let markerO = GMSMarker()
        markerO.icon = #imageLiteral(resourceName: "startLocation")
        markerO.position = CLLocationCoordinate2DMake(self.userData.pickUpLat,self.userData.pickUpLong)
        markerO.title = self.userData.source
        //marker.snippet = place.
        markerO.map = self.googleMap
        
        let markerD = GMSMarker()
        markerD.icon = #imageLiteral(resourceName: "endLocation")
        markerD.position = CLLocationCoordinate2DMake(self.userData.dropOffLat,self.userData.dropOffLong)
        markerD.title = self.userData.destination
        //marker.snippet = place.
        markerD.map = self.googleMap
        var bounds = GMSCoordinateBounds()
       // print("origin is ...\(origin! )")
        
        var request = URLRequest(url: URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(self.userData.pickUpLat),\(self.userData.pickUpLong)&destination=\(self.userData.dropOffLat),\(self.userData.dropOffLong)&key=AIzaSyCWr4E00ZgbontTul8YxD6XZm8NV1OL7UU")!)
        
        //AIzaSyBMqE0SP6LVMf36vAAT-jzF-k-h1X9rK8k :- old direction key
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        //request.httpMethod = "GET"
       // let session = URLSession.shared
        
        session.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                print("json:",json)
                
                
                if let routes = json["routes"] as? [[String : AnyObject]] {
                    if routes.count > 0 {
                        var first = routes.first
                        if let legs = first!["legs"] as? [[String : AnyObject]] {
                            // let fullPath : GMSPolyline = GMSPolyline()
                            for leg in legs {
                                if let steps = leg["steps"] as? [[String : AnyObject]] {
                                    for step in steps {
                                        if let polyline = step["polyline"] as? [String : AnyObject] {
                                            if let points = polyline["points"] as? String {
                                                //                                      fullPath.path = GMSMutablePath(fromEncodedPath: points)
                                                DispatchQueue.main.sync {
                                                    let path: GMSPath = GMSPath(fromEncodedPath: points)!
                                                    let routePolyline = GMSPolyline(path: path)
                                                    routePolyline.strokeColor = .black
                                                    routePolyline.strokeWidth = 4
                                                    routePolyline.map = self.googleMap
                                                    
                                                    print(path.count())
                                                    for index in 1...path.count() {
                                                        bounds = bounds.includingCoordinate(path.coordinate(at: index))
                                                    }
                                                    self.googleMap.animate(with: GMSCameraUpdate.fit(bounds))
                                                    //  self.googleMap.animate(toZoom: 14)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
    }
    
    } catch let myJSONError {
    print(myJSONError)
    }
    }.resume()
    }

    @IBAction func actionOnBackBtn(_ sender: Any) {
        
       self.dismiss(animated: true, completion: nil)
        
    }
    


}
