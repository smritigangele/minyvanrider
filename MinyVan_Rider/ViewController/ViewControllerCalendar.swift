//
//  ViewControllerCalendar.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 19/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import UIKit

import JTAppleCalendar

var gblVar = ""

class ViewControllerCalendar: UIViewController {
     let selectedClr = UIColor(red: 45/255, green: 175/255, blue: 185/255, alpha: 1)
    static let redClr = UIColor(red: 238/255, green: 61/255, blue: 576/255, alpha: 1)
    let formatter = DateFormatter()
    var tripId: Int!
    var newDate = ""
    var selectedDates = [String]()
     var dateStrings = [String]()
    let userCalendar =  Calendar.current
     var result:Double!
    let todaysDate = Date()
    let todaysDateColor = UIColor.white
    let selectedDateColor = UIColor.white
    let thisMonthColor = UIColor.black
    let notThisMonthColor = UIColor.lightGray
    @IBOutlet weak var calenderView: JTAppleCalendarView!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    //weak var delegate: ViewControllerBDelegate?
    
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet var weekdayLbl: UILabel!

    var text1: String? = nil
    var hideStatusBar = false
    var testCalendar = Calendar.current
    var visibleDates: DateSegmentInfo?
    var startDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         calenderView.allowsMultipleSelection = true
         setupCalenderView()

         //let date = Date()
  
       calenderView.scrollToDate(Date())
       calenderView.selectDates([Date()])
        // getDay(text: date)
        
    }
    
    var rangeSelectedDates: [Date] = []
    func didStartRangeSelecting(cellState: CellState, cell: JTAppleCell?, date: Date) {
      
        rangeSelectedDates = calenderView.selectedDates
        
        if rangeSelectedDates.contains(date){
                let dateRange = calenderView.generateDateRange(from: rangeSelectedDates.first ?? date, to: date)
                for aDate in dateRange {
                  //  if !rangeSelectedDates.contains(aDate) {
                      //  rangeSelectedDates.append(aDate)
                   // }
                }
            calenderView.selectDates([rangeSelectedDates.first!], triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: true)
        
            } else {
            
                let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)! + 1
                let lastIndex = rangeSelectedDates.endIndex
     rangeSelectedDates.removeSubrange(indexOfNewlySelectedDate..<lastIndex)
        }
       
        let dateformatter = DateFormatter()
        
        dateformatter.dateFormat = "yyyy-MM-dd"
        
        for date in rangeSelectedDates {
          let now = dateformatter.string(from: date)
            dateStrings.append(now)
        }
        print(rangeSelectedDates)
    }

    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "yyyy"
        self.year.text! = self.formatter.string(from: date)
        self.formatter.dateFormat = "MMMM"
        self.month.text! = self.formatter.string(from: date)
        
    }

   func setupCalenderView(){
            calenderView.minimumLineSpacing = 0
            calenderView.minimumInteritemSpacing = 0
        }
    
    
    @IBAction func actionONPreviousMonth(_ sender: Any) {
        
      //calenderView.scrollToSegment(.previous)
        let next = userCalendar.date(byAdding: .year, value: -1, to: calenderView.selectedDates[0])!
        calenderView.scrollToDate(next, animateScroll: false)
        calenderView.deselectAllDates()
        calenderView.selectDates([next], triggerSelectionDelegate: true)
        moveToPreviousMonth()
    }
    
    func moveToPreviousMonth(){
        let next1 = userCalendar.date(byAdding: .month, value: -1, to: calenderView.selectedDates[0])!
        calenderView.scrollToDate(next1, animateScroll: false)
        calenderView.selectDates([next1], triggerSelectionDelegate: true)
    }
    
    @IBAction func actionOnNextMonth(_ sender: Any) {
        
        let next = userCalendar.date(byAdding: .year, value: 1, to: calenderView.selectedDates[0])!
        calenderView.scrollToDate(next, animateScroll: false)
        calenderView.deselectAllDates()
        calenderView.selectDates([next], triggerSelectionDelegate: true)
        
        let next1 = userCalendar.date(byAdding: .month, value: 1, to: calenderView.selectedDates[0])!
        calenderView.selectDates([next1], triggerSelectionDelegate: true)
        calenderView.scrollToDate(next1, animateScroll: false)
    }
    
    func handleTextColorOfCell(view: JTAppleCell? ,cell: CellState){
        
        guard let validCell = view as? CustomCell  else{ return }
        formatter.dateFormat = "yyyy MM dd"
        let todaysDateString = formatter.string(from: todaysDate)
        let monthDateString = formatter.string(from: cell.date)
        // print("cellState \(cellState.isSelected)")
        
        if todaysDateString == monthDateString {
            validCell.dateLbl.textColor = todaysDateColor
        }
            
        else {
        if cell.isSelected{
            
            validCell.selectedView.layer.cornerRadius = 20
            validCell.selectedView.clipsToBounds = true
            validCell.dateLbl.textColor = selectedDateColor
        }
            
        else{
            if cell.dateBelongsTo == .thisMonth{
                validCell.dateLbl.textColor = thisMonthColor
                if cell.date.isWeekend == true{
                 validCell.dateLbl.textColor = notThisMonthColor
                }
            }
            else{
                validCell.dateLbl.textColor = notThisMonthColor
            }
        }
        }
    }
    func handleSelectedCell(view:JTAppleCell?, cellState: CellState)
    {
        guard let validCell = view as? CustomCell  else{ return }
        if validCell.isSelected{
            if cellState.date.isWeekend == true{
                validCell.dateLbl.textColor = notThisMonthColor
                 validCell.selectedView.isHidden = true
            }
            else{
                 validCell.selectedView.isHidden = false
            }
        }
        else{
            validCell.selectedView.isHidden = true
        }
        
    }
    
    @IBAction func actionOnCancle(_ sender: Any) {
          rangeSelectedDates.removeAll()
          selectedDates.removeAll()
          dateStrings.removeAll()
          print("range :- \(rangeSelectedDates)")
          print("selected dates array :- \(selectedDates)")
          print("date string array :- \(dateStrings)")
          self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func actionOnOK(_ sender: Any) {
         print("range of dates ;- \(dateStrings)")
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale
        for date in rangeSelectedDates{
            //let date1: Date? = df.date(from: date)
            let since1970: TimeInterval? = date.timeIntervalSince1970
            print("result is :-\(since1970!)")
            let someValue: Float = Float(since1970!)
           let finalValue = someValue.cleanValue
            //let longValue = Int(since1970)
            newDate = "\(finalValue)"
            selectedDates.append(newDate)
        }
        
        print("final string; - \(selectedDates)")

        if self.selectedDates.count == 0{
            self.createAlert(title: "Alert!", body: "Please select the date")
        }
        let bookMultipleTrip = BookMultipleTripVM()
        bookMultipleTrip.tripId = tripId
        bookMultipleTrip.dates = self.selectedDates
        bookMultipleTrip.tripBookingStatus = "CUSTOM"
        
        APIManger.bookMultipleTrip(view: self, bookRide: bookMultipleTrip){
            (completion: ForgotPassDTO) in
            print(completion)
            self.createAlert(title: "Booking Completed", body: completion.reason)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = self.storyboard?.instantiateViewController(withIdentifier: "Home") as! BookRideViewController
            let presentingVC = self.presentingViewController
            self.dismiss(animated: false, completion: { () -> Void   in
                presentingVC!.present(destinationController, animated: true, completion: nil)
            })
            self.selectedDates.removeAll()
            self.dateStrings.removeAll()
            self.rangeSelectedDates.removeAll()
            self.navigationController?.popToRootViewController(animated: true)
            
        }
        
    }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }

}

extension ViewControllerCalendar: JTAppleCalendarViewDataSource{
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale =  Calendar.current.locale

        print(Date().dayOfWeek()!)
        let startDate = userCalendar.date(byAdding: Calendar.Component.year, value: -50, to: Date())
        let endDate = userCalendar.date(byAdding: Calendar.Component.year, value: 50, to: Date())
        
        let parameters = ConfigurationParameters(startDate: startDate!, endDate: endDate!,numberOfRows: 5,
            calendar: Calendar.current,
            generateInDates:  .forFirstMonthOnly,
            generateOutDates: .tillEndOfRow,
            firstDayOfWeek:   .sunday)
        
        return parameters
        
    }
    
    func getDay(text: Date){
        
        formatter.string(from: text)
        let day = Calendar.current.component(.day, from: text)
        let year = Calendar.current.component(.year, from: text)
        let monthLbl = Calendar.current.component(.month, from: text)
        self.dateLbl.text! = "\(day)"
        let dayofWeek1 = text.dayOfWeek()!
        print("month :- \(monthLbl)")
        self.weekdayLbl.text! = dayofWeek1
        self.yearLbl.text! = String(year)
        
//        let date = Date()
//        var myCalendar2 = Calendar(identifier: .gregorian)
//        myCalendar2.timeZone = TimeZone(identifier: "America/Vancouver")!
//        myCalendar2.locale = Locale(identifier: "en_US")
//
        //let aFormatter2 = DateFormatter()
//        formatter.dateFormat = "yyyy MM dd"
//        formatter.timeZone = myCalendar2.timeZone
//        formatter.locale = myCalendar2.locale
        
        print(formatter.string(from: text))
        print(Date().dayOfWeek()!)
        
         formatter.dateFormat = "MMMM"
        let nameOfMonth = formatter.string(from: text)
        self.month.text! = formatter.string(from: text)
        self.formatter.dateFormat = "yyyy"
        self.year.text! = self.formatter.string(from: text)
        
    }
    
}

extension ViewControllerCalendar: JTAppleCalendarViewDelegate{
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        let myCustomCell = cell as! CustomCell
//
        // Setup Cell text
        myCustomCell.dateLbl.text = cellState.text
//
        // Setup text color
        if cellState.dateBelongsTo == .thisMonth {
            myCustomCell.dateLbl.textColor = thisMonthColor
//            if cellState.date == .sunday && cellState.day == .saturday{
//                myCustomCell.dateLbl.textColor = notThisMonthColor
//            }
        } else {
            myCustomCell.dateLbl.textColor = notThisMonthColor
        }
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.dateLbl.text = cellState.text
        handleSelectedCell(view: cell, cellState: cellState)
        handleTextColorOfCell(view: cell, cell: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        getDay(text: date)
        if date.isWeekend == true{
          self.createAlert(title: "OOPS!", body: "You cann't book ride on weekends")
        }
        else{
        didStartRangeSelecting(cellState: cellState, cell: cell, date: date)
        }
        handleSelectedCell(view: cell, cellState: cellState)
        handleTextColorOfCell(view: cell, cell: cellState)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        if rangeSelectedDates.count == 0{
            handleSelectedCell(view: cell, cellState: cellState)
            handleTextColorOfCell(view: cell, cell: cellState)
        }
        else{
           let indexOfNewlySelectedDate = rangeSelectedDates.index(of: date)!
          print(indexOfNewlySelectedDate)
          rangeSelectedDates.remove(at: indexOfNewlySelectedDate)
          print(rangeSelectedDates)
          handleSelectedCell(view: cell, cellState: cellState)
          handleTextColorOfCell(view: cell, cell: cellState)
        }
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
       setupViewsOfCalendar(from: visibleDates)
    }
    
}

extension Date {
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
    func monthAsString() -> String {
        let dateformttr = DateFormatter()
        dateformttr.setLocalizedDateFormatFromTemplate("MMM")
        return dateformttr.string(from: self)
    }
        var isWeekend: Bool {
            return NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!.isDateInWeekend(self)
        }
}

protocol ViewControllerBDelegate: class {
    
    func textChanged(text:String?)
    
}
extension Float
{
    var cleanValue: String
    {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension ViewControllerCalendar{
    
 func getNumberOfDaysInMonth (month : Int , Year : Int) -> Int{
    
    var dateComponents = DateComponents()
    dateComponents.year = Year
    dateComponents.month = month
    let calendar = Calendar.current
    let date = calendar.date(from: dateComponents)
    
    let range = calendar.range(of: .day, in: .month, for: date!)
    //rangeOfUnit(.Day, inUnit: .Month, forDate: date)
    
    let numDays = range?.count
    var numberOfSundays = 0
    let dateFormatter = DateFormatter()
    
    for day in 0...numDays! {
        
        dateComponents.day = day
    
        let date1 = calendar.date(from: dateComponents)
        dateFormatter.dateFormat = "EEEE"
    let dayOfWeek = dateFormatter.string(from: date1!) // Get day of week
    
        if dayOfWeek == "Sunday" { // Check if it's a Sunday
            numberOfSundays += 1
        }
    }
    
    return numDays! - numberOfSundays
  
}
}
