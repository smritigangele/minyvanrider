//
//  ActiveRidesCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class ActiveRidesCell: UITableViewCell{
    

    @IBOutlet var dateLbl: UILabel!
    
    @IBOutlet var sourceLbl: UILabel!
    
    @IBOutlet var destinationLbl: UILabel!
    
    @IBOutlet var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
