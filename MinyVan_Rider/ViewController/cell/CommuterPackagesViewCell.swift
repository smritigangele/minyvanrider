//
//  CommuterPackagesViewCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class CommuterPackagesViewCell: UITableViewCell{
   
    @IBOutlet var packageName: UILabel!
    
    @IBOutlet var priceLbl: UILabel!
    
    @IBOutlet var totalNoOfTrips: UILabel!
    
    @IBOutlet var totalNoOfDays: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
