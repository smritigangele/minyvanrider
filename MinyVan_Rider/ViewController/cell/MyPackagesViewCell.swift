//
//  MyPackagesViewCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class MyPackagesViewCell: UITableViewCell{
    
    @IBOutlet var viewForList: UIView!
    
    @IBOutlet var packageName: UILabel!
    
    @IBOutlet var noOfTripsLeft: UILabel!
    
    @IBOutlet var expiryDate: UILabel!
    
    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var shareBtn: UIButton!
    
    
    @IBOutlet var pkgNameForCompletion: UILabel!
    
    @IBOutlet var leftTripsForCompletion: UILabel!
    
    @IBOutlet var expiryDateForCompletion: UILabel!
    
    @IBOutlet var priceForCompletion: UILabel!
    
    
    @IBOutlet var pkgNameForExpired: UILabel!
    
    
    @IBOutlet var priceLblForExpired: UILabel!
    
    @IBOutlet var leftTripsForExpired: UILabel!
    
    @IBOutlet var dateForExpiredPkg: UILabel!
    
    override func awakeFromNib() {
        
        
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
   
}
