//
//  MyRidesCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 11/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation

import UIKit

class MyRidesCell: UITableViewCell{
    
    @IBOutlet var sourceLbl: UILabel!
    @IBOutlet var packageNameLbl: UILabel!
    
    @IBOutlet var destinationLbl: UILabel!
    
    @IBOutlet var DriverTipLbl: UILabel!
    
    @IBOutlet var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

