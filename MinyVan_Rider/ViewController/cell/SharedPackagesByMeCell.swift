//
//  SharedPackagesCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class SharedPackagesByMeCell: UITableViewCell{
    
    @IBOutlet var packageName: UILabel!
    
    @IBOutlet var noOfRideShared: UILabel!
    
    @IBOutlet var expiryLbl: UILabel!

    @IBOutlet var priceLbl: UILabel!
    
    @IBOutlet var sharedWithNameLbl: UILabel!
    
    @IBOutlet var mobileNumberLbl: UILabel!
    
    @IBOutlet var shareduserEmail: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
