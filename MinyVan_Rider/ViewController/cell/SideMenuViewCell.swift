//
//  SideMenuViewCell.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 08/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import UIKit

class SideMenuViewCell: UITableViewCell{
    
    @IBOutlet var nameLbl: UILabel!
    
    @IBOutlet var imageOfNameLbl: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
