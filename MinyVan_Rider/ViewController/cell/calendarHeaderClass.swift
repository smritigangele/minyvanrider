//
//  calendarHeaderClass.swift
//  MinyVan_Rider
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/12/17.
//  Copyright © 2017 newOrganization. All rights reserved.
//

import Foundation
import JTAppleCalendar

class calendarHeaderClass: JTAppleCollectionReusableView {
    @IBOutlet var year: UILabel!
    @IBOutlet var month: UILabel!
}
